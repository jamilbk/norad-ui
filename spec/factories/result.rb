# frozen_string_literal: true

FactoryGirl.define do
  factory :result, class: Result do
    status 'pass'
    ignored false
  end

  trait :failed do
    status 'fail'
  end

  trait :errored do
    status 'error'
  end

  trait :passed do
    status 'pass'
  end

  trait :ignored do
    ignored true
    status nil
  end
end
