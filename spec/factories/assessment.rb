# frozen_string_literal: true

FactoryGirl.define do
  factory :assessment, class: Assessment do
    sequence(:machine_id) { |n| n }

    trait :with_failed_result do
      initialize_with do
        new(machine_id: machine_id, results: [build_stubbed(:result, :failed)])
      end
    end

    trait :with_errored_result do
      initialize_with do
        new(machine_id: machine_id, results: [build_stubbed(:result, :errored)])
      end
    end

    trait :with_passed_result do
      initialize_with do
        new(machine_id: machine_id, results: [build_stubbed(:result, :passed)])
      end
    end

    trait :with_ignored_result do
      initialize_with do
        new(machine_id: machine_id, results: [build_stubbed(:result, :ignored)])
      end
    end

    trait :with_incomplete_result do
      initialize_with do
        new(machine_id: machine_id, results: [])
      end
    end
  end
end
