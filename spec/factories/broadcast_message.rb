# frozen_string_literal: true

FactoryGirl.define do
  factory :broadcast_message do
    text { 'We will be experiencing some downtime tonight' }
  end
end
