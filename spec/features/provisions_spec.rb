# frozen_string_literal: true

require 'rails_helper'

feature 'Provisions functionality' do
  before :each do
    valid_login
    @req_group = create_requirement_group_helper
    td = find('td', text: @req_group)
    td.find('a').click
    @req = create_requirement_helper
    td = find('td', text: @req)
    td.find(:xpath, '..').find('a[title="Requirement Settings"]').click
  end

  def add_provision(name)
    find('select.security-test-list ~ span.select2').click
    find('.select2-results').find('li', text: name).click
    click_on 'Add Security Tests'
  end

  scenario 'add a provision', js: true do
    name = 'vuls'
    add_provision(name)

    table = find('table#enabled-containers')
    td = table.find('td', text: name)
    expect(td).to be_present
    expect(page).to_not have_content("You don't have permission to add this provision")
  end

  scenario 'adding a provision without permissions to do so', js: true do
    valid_login('user_without_access')

    visit requirement_groups_path
    click_on @req_group

    td = find('td', text: @req)
    td.find(:xpath, '..').find('a[title="Requirement Settings"]').click
    find('.security-test-list ~ span.select2').click
    find('.select2-results').find('li', text: /vuls\z/).click
    click_on 'Add Security Tests'

    expect(page).to have_content("You don't have permission to add this provision")
  end

  scenario 'delete a provision', js: true do
    name = 'zap-passive'
    add_provision(name)

    table = find('table#enabled-containers')
    td = table.find('td', text: name)
    expect(td).to be_present
    td.find(:xpath, '..').find('a[data-method="delete"]').click
    expect(page).not_to have_selector('td', text: name)
  end
end
