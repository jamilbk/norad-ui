# frozen_string_literal: true

require 'rails_helper'

feature 'User functionality', js: true do
  context 'when sso auth' do
    before :each do
      valid_login
    end

    scenario 'show settings for current user' do
      visit users_path
      expect(page).to have_content('API Token')
    end

    scenario "show warning for accessing another user's info" do
      visit user_path(-1)
      expect(page).to have_content('How dare you')
    end

    scenario 'recreate api token', js: true do
      visit users_path
      find('button.btn-box-tool').click
      old_token = find('#api-token').text
      accept_confirm do
        find("a[href='#{user_api_token_path('testuser')}']").click
      end

      find('button.btn-box-tool').click
      new_token = find('#api-token').text

      expect(page).to have_content('API Token Recreated!')
      expect(old_token).not_to eq(new_token)
    end
  end
end

feature 'User update', js: true do
  before :each do
    valid_login
  end

  scenario 'update user' do
    visit users_path
    first_name = (0...40).map { ('a'..'z').to_a[rand(26)] }.join
    last_name = (0...40).map { ('a'..'z').to_a[rand(26)] }.join
    find(:css, 'i.fa.fa-edit').click
    fill_in 'user[firstname]', with: first_name
    fill_in 'user[lastname]', with: last_name
    click_on 'Save'
    expect(page).to have_content(first_name)
    expect(page).to have_content(last_name)
  end
end
