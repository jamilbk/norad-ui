# frozen_string_literal: true

require 'rails_helper'

feature 'Jira Export Queue functionality' do
  before :each do
    valid_login
    @org_slug = create_org_helper
    visit settings_organization_path(@org_slug)
  end

  def add_jira_export_queue
    click_on 'add-result-export-queue'
    wait_for_ajax
    select('Jira', from: 'queue-type')
    fill_in 'jira_export_queue[custom_jira_configuration_attributes][title]', with: 'Demo Result Export Location'
    fill_in 'jira_export_queue[custom_jira_configuration_attributes][site_url]', with: 'https://jira.ciscodemo.invalid'
    fill_in 'jira_export_queue[custom_jira_configuration_attributes][project_key]', with: 'PKey-123'
    fill_in 'jira_export_queue[custom_jira_configuration_attributes][username]', with: 'testuser'
    fill_in 'jira_export_queue[custom_jira_configuration_attributes][password]', with: 'password'
    click_on 'save-export-queue'
  end

  scenario 'Add result export location', js: true do
    add_jira_export_queue
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'Demo Result Export Location')
  end

  scenario 'Edit result export location auto-sync', js: true do
    add_jira_export_queue
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'true')

    within(:css, '#queue-table') do
      find('a[href$=edit]').click
    end
    wait_for_ajax
    select('false', from: 'jira_export_queue[auto_sync]')
    click_on 'save-export-queue'
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'false')
  end

  scenario 'Edit result export location detail', js: true do
    add_jira_export_queue
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'true')

    within(:css, '#queue-table') do
      find('a[href$=edit]').click
    end
    wait_for_ajax
    fill_in 'jira_export_queue[custom_jira_configuration_attributes][title]', with: 'TitleEdit'
    click_on 'save-export-queue'
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'TitleEdit')
  end

  scenario 'Delete result export location', js: true do
    add_jira_export_queue
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'Demo Result Export Location')

    within(:css, '#queue-table') do
      find('.delete-action').click
    end
    wait_for_ajax
    expect(page).not_to have_css('#queue-table > tbody > tr > td', text: 'Demo Result Export Location')
  end
end
