# frozen_string_literal: true

require 'rails_helper'

feature 'Service Management', js: true do
  before :each do
    valid_login
    @org_slug = create_org_helper
    @machine = create_machine_helper(@org_slug)
    visit machine_services_path(@machine)
  end

  def add_ssh_service
    click_on 'add-service'
    wait_for_ajax
    sleep 0.5
    click_on 'Looking for the old form?'
    sleep 0.25
    fill_in 'service[port]', with: '22'
    fill_in 'service[name]', with: 'Capybara SSH Service'
    fill_in 'service[description]', with: 'This service was added by Capybara'
    find('#port-type').find(:xpath, 'option[1]').select_option
    click_on 'Save'
  end

  def add_amqp_service
    click_on 'add-service'
    wait_for_ajax
    sleep 0.5
    click_on 'Looking for the old form?'
    sleep 0.25
    fill_in 'service[port]', with: '5672'
    fill_in 'service[name]', with: 'Capybara AMQP Service'
    fill_in 'service[description]', with: 'This AMQP service was added by Capybara'
    find('#port-type').find(:xpath, 'option[1]').select_option
    click_on 'Save'
  end

  def add_postgres_service
    click_on 'add-service'
    wait_for_ajax
    sleep 0.5
    click_on 'Looking for the old form?'
    sleep 0.25
    fill_in 'service[name]', with: 'Postgresql Database'
    fill_in 'service[description]', with: 'Norad Rails Database.'
    find('#port-type').find(:xpath, 'option[1]').select_option
    fill_in 'service[port]', with: '5432'
    find('#service-type').find(:xpath, 'option[1]').select_option
    click_on 'Save'
  end

  context 'when a machine has a failed service discovery' do
    let(:error_message) { 'A test error message' }

    before(:each) do
      discovery = double
      expected_time = '2010-01-01 12:30:00 UTC'.to_datetime
      displayed_time = expected_time.strftime('%b %d, %Y @ %I:%M:%S %p %Z')
      allow(discovery).to receive(:error_message).and_return(error_message)
      allow(discovery).to receive(:created_at).and_return expected_time
      allow_any_instance_of(Machine).to receive(:latest_service_discovery).and_return(discovery)
      @expected_content =
        "The latest service discovery attempt for this machine initiated at #{displayed_time} failed "\
        "with the following error: #{error_message} If you've already added services to this machine you may "\
        'disregard this message.'
    end

    it 'displays the error message in the services box' do
      visit machine_services_path(@machine)
      expect(page).to have_content(@expected_content)
    end
  end

  context 'when a machine does not have an assigned ssh key' do
    before(:each) do
      visit settings_machine_path(@machine)
      expect(page).to have_content('No Keys available to assign to this machine.')
    end

    scenario 'adding a SSH service to a machine redirects to the settings page' do
      visit machine_services_path(@machine)
      add_ssh_service
      expect(current_path).to eq(settings_machine_path(@machine))
      expect(page).to have_content(
        "You have added an SSH Service to machine \"#{@machine.name}\", "\
        'but no SSH Key Pair is available. It is recommended to assign one.'
      )
    end
  end

  context 'when a machine has an assigned ssh key' do
    before(:each) do
      add_new_ssh_key_pair(@org_slug)
      visit settings_machine_path(@machine)
      click_on 'Assign Key To Machine'
    end

    context 'when a machine has a ssh service' do
      before(:each) do
        visit machine_services_path(@machine)
        add_ssh_service
      end

      scenario 'delete a service from a machine' do
        expect(page).to have_content('Capybara SSH Service')
        accept_confirm do
          find('i.fa.fa-close.action-icon').click
        end
        # If this test consistently fails, try sleeping here. wait_for_ajax caused the execution to
        # timeout for me.
        # sleep 1
        expect(page).not_to have_content('Capybara SSH Service')
      end

      scenario 'update a service on a machine' do
        expect(page).to have_content('Capybara SSH Service')
        find(:css, 'i.fa.fa-edit').click
        fill_in 'service[name]', with: 'Updated SSH Service'
        fill_in 'service[port]', with: '2222'
        click_on 'Save'
        expect(page).not_to have_content('Capybara SSH Service')
        expect(page).to have_content('Updated SSH Service')
      end

      scenario 'update a service with duplicate port' do
        add_amqp_service
        first(:css, 'i.fa.fa-edit').click
        fill_in 'service[port]', with: '5672'
        click_on 'Save'

        expect(page).to have_content('Port has already been taken.')
      end
    end
  end

  scenario 'add postgres service and check service type' do
    add_postgres_service
    expect(page).to have_content('postgresql')
  end
end
