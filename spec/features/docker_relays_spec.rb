# frozen_string_literal: true

require 'rails_helper'

feature 'Docker Relays', js: true do
  include OrganizationTestHelper

  before :each do
    valid_login
    @org_slug = create_org_helper
    visit organization_relays_path(@org_slug)
  end

  scenario 'verify docker relay' do
    create_relay_helper(@org_slug)
    visit organization_relays_path(@org_slug)
    check_box = first('input.verify')
    check_box.click

    expect(page).to have_content("Relay[#{check_box['data-id']}] successfully updated!")
  end

  context 'view relays as org reader' do
    before(:each) do
      reader = 'readeruser'
      valid_login(reader)
      valid_login
      add_member_to_org(@org_slug, reader, 'Reader')
      valid_login(reader)
    end

    scenario 'shows the unauthorized message' do
      visit organization_relays_path(@org_slug)
      expect(page).to have_content('You are not authorized to view the Relays for this Organization')
    end
  end

  # TODO: This spec is not complete.
  # To write a proper feature spec, we need a mechanism of bumping a relay offline.
  # Since check_for_offline_relays is a rake task, and 1 hour must elapse, we
  # need to introduce some form of mocking either in the UI or the API.
  scenario 'verify relays page displays proper warnings' do
    warning_message = 'Verified relays are offline.'
    create_relay_helper(@org_slug)
    visit organization_relays_path(@org_slug)
    expect(page).not_to have_content(warning_message)
    # Force relay to transition to offline state.
    visit organization_relays_path(@org_slug)
    # expect(page).to have_content(warning_message)
  end

  scenario 'enable auto verify new docker relays' do
    find('#auto-verify').click

    expect(page).to have_content('Org config successfully updated!')
  end

  scenario 'enable relay ssh key' do
    find('#ssh-key').click
    find('#toggle-relay-ssh-key').click

    expect(page).to have_content('Org config successfully updated!')
  end

  scenario 'outdated relay' do
    # Poltergeist runs in another thread which is why I think traditional stubbing doesn't work here
    class DockerRelay
      def outdated
        true
      end

      def last_reported_version
        nil
      end
    end

    create_relay_helper(@org_slug)
    visit organization_relays_path(@org_slug)

    expect(page).to have_css('label[title*="Newer Relay version available."]')
    expect(page).to have_content 'Unreported'
  end
end
