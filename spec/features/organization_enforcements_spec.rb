# frozen_string_literal: true

require 'rails_helper'

feature 'Organization Enforcement functionality' do
  before :each do
    valid_login
    @org_slug = create_org_helper
    @req_group_name = create_requirement_group_helper
    visit organization_security_tests_path(@org_slug)
  end

  define_method :add_requirement_group do |name|
    find('select.req-group-list ~ span.select2').click
    find('.select2-results').find('li', text: name).click
    click_on 'add-requirements'
  end

  define_method :add_relevant_security_test do
    find('select.security-test-list ~ span.select2').click
    find('.select2-results').find('li', text: /\Avuls\Z/).click
    click_on 'Add Security Tests'
  end

  define_method :create_configurable_requirement_group do
    req_group = create_requirement_group_helper
    td = find('td', text: req_group)
    td.find('a').click

    req = create_requirement_helper
    td = find('td', text: req)
    td.find(:xpath, '..').find('a[title="Requirement Settings"]').click

    add_relevant_security_tests

    visit organization_security_tests_path(@org_slug)
    req_group
  end

  scenario 'add a requirement group', js: true do
    expect(page).not_to have_selector('.req-group')
    add_requirement_group @req_group_name
    expect(page).to have_selector('.req-group > div', text: @req_group_name)
  end

  scenario 'delete a requirement group', js: true do
    expect(page).not_to have_selector('.req-group')
    add_requirement_group @req_group_name
    expect(page).to have_selector('.req-group > div', text: @req_group_name)

    accept_confirm do
      find('.req-group > div.req-group-actions > a.btn-danger', text: 'Remove').click
    end
    expect(page).to_not have_selector('.req-group > div', text: @req_group_name)
  end

  skip 'create config from requirement', js: true do
    skip 'The test environment does not currently have any configurable security tests'
    expect(page).not_to have_selector('.req-group')
    req_group = create_configurable_requirement_group
    add_requirement_group(req_group)
    find('.req-group > .req-group-name > a.req-list-toggle').click

    row = first('.req-list table').first('tbody tr')
    container_name = row.first('td').text
    row.find('a.create-config').click

    expect(page).to have_selector('#enabled-containers td', text: container_name)
    expect(row).to have_selector('.create-config', text: 'Config Created')
  end

  skip 'disable create-config button when config exists', js: true do
    skip 'The test environment does not currently have any configurable security tests'
    expect(page).not_to have_selector('.req-group')
    req_group = create_configurable_requirement_group
    add_requirement_group(req_group)
    find('.req-group > .req-group-name > a.req-list-toggle').click

    row = first('.req-list table').first('tbody tr')
    container_name = row.first('td').text
    row.find('a.create-config').click

    expect(page).to have_selector('#enabled-containers td', text: container_name)
    expect(row).to have_selector('.create-config', text: 'Config Created')

    # remove requirement group
    accept_confirm do
      find('.req-group > div.req-group-actions > a.btn-danger', text: 'Remove').click
    end
    expect(page).to_not have_selector('.req-group')
    expect(page).to have_selector('#enabled-containers td', text: container_name)

    # add requirement group back
    add_requirement_group(req_group)
    find('.req-group > .req-group-name > a.req-list-toggle').click
    expect(first('.req-list table').first('tbody tr')).to have_selector('a.create-config', text: 'Config Created')
  end
end
