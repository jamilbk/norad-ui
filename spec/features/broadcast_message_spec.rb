# frozen_string_literal: true

require 'rails_helper'

feature 'Broadcast Message', js: true do
  include OrganizationTestHelper

  scenario 'Broadcast message lifecycle: creation, creation of message with different severity, deactivation' do
    BroadcastMessage.create_and_activate(text: 'We will be experiencing some downtime tonight')

    valid_login

    expect(page).to have_content('We will be experiencing some downtime tonight')
    expect(page).to_not have_content('no more downtime tonight, deployment finished')

    expect(page).to have_css('div.broadcast-message.alert-warning')
    expect(page).to_not have_css('div.broadcast-message.alert-info')

    BroadcastMessage.create_and_activate(text: 'no more downtime tonight, deployment finished', severity: 'info')

    visit organizations_path

    expect(page).to_not have_content('We will be experiencing some downtime tonight')
    expect(page).to have_content('no more downtime tonight, deployment finished')

    expect(page).to_not have_css('div.broadcast-message.alert-warning')
    expect(page).to have_css('div.broadcast-message.alert-info')

    BroadcastMessage.last.update_attributes!(live: false)

    visit root_path

    expect(page).to_not have_content('We will be experiencing some downtime tonight')
    expect(page).to_not have_content('no more downtime tonight, deployment finished')

    expect(page).to_not have_css('div.broadcast-message.alert-warning')
    expect(page).to_not have_css('div.broadcast-message.alert-info')
  end
end
