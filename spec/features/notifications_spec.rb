# frozen_string_literal: true

require 'rails_helper'

feature 'Notifications' do
  before :each do
    valid_login
    @org_slug = create_org_helper
  end

  context 'for organizations' do
    scenario 'disables and enables notification', js: true do
      # enabled by default
      visit settings_organization_path(@org_slug)
      checkbox = find('input[type=checkbox]', class: 'notification_channel_enabled')
      expect(checkbox).to be_checked

      # disable notification
      checkbox.click
      expect(page).to have_content 'Org Notification Channel successfully updated!'
      visit settings_organization_path(@org_slug)
      expect(checkbox).not_to be_checked

      # enable again
      checkbox.click
      expect(page).to have_content 'Org Notification Channel successfully updated!'
      visit settings_organization_path(@org_slug)
      expect(checkbox).to be_checked
    end
  end
end
