# frozen_string_literal: true

require 'rails_helper'

feature 'Infosec Export Queue functionality' do
  before :each do
    valid_login
    @org_slug = create_org_helper
    visit settings_organization_path(@org_slug)
  end

  def add_infosec_export_queue
    click_on 'add-result-export-queue'
    wait_for_ajax
    select('Cisco InfoSec', from: 'queue-type')
    click_on 'save-export-queue'
  end

  scenario 'Add result export location', js: true do
    add_infosec_export_queue
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'Cisco Infosec')
  end

  scenario 'Edit result export location', js: true do
    add_infosec_export_queue
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'true')

    within(:css, '#queue-table') do
      find('a[href$=edit]').click
    end
    wait_for_ajax
    select('false', from: 'infosec_export_queue[auto_sync]')
    click_on 'save-export-queue'
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'false')
  end

  scenario 'Delete result export location', js: true do
    add_infosec_export_queue
    expect(page).to have_css('#queue-table > tbody > tr > td', text: 'Cisco Infosec')

    within(:css, '#queue-table') do
      find('.delete-action').click
    end
    wait_for_ajax
    expect(page).not_to have_css('#queue-table > tbody > tr > td', text: 'Cisco Infosec')
  end
end
