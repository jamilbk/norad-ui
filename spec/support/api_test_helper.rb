# frozen_string_literal: true

require 'net/http'
module ApiTestHelper
  class << self
    def begin_transaction
      helper_get :begin_transaction
    end

    def rollback_transaction
      helper_get :rollback_transaction
    end

    def mail_deliveries
      helper_get :mail_deliveries
    end

    def last_email_confirmation_token
      JSON.parse(helper_get(:last_email_confirmation_token))['token']
    end

    def last_password_reset_token
      JSON.parse(helper_get(:last_password_reset_token))['token']
    end

    def env=(data = {})
      helper_post :set_env, data
    end

    private

    def helper_get(path)
      Net::HTTP.get URI("#{endpoint}#{path}")
    end

    def helper_post(path, data)
      # ruby 2.4.0 makes this much simpler, e.g. Net::HTTP.post(uri, data, headers)
      uri = URI("#{endpoint}#{path}")
      http = Net::HTTP.new(uri.host, uri.port)
      req = Net::HTTP::Post.new uri.request_uri, 'Content-Type' => 'application/json'
      req.body = { env: data }.to_json
      http.request req
    end

    def endpoint
      "#{ENV['API_URL'].gsub(/v1/, '')}client_test_route_helpers/"
    end
  end
end
