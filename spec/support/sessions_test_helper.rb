# frozen_string_literal: true

module SessionsTestHelper
  def mock_auth_type(type)
    case type.to_s
    when 'reverse_proxy'
      set_sso_domain
    else
      unset_sso_domain
    end
  end

  def timestamped_uid
    "ui-test-#{Time.now.to_i}"
  end

  def timestamped_email
    "#{timestamped_uid}@norad.test"
  end

  def local_signup(user_atts = {})
    logout
    visit new_user_path
    fill_in 'user[email]', with: user_atts[:email] || timestamped_email
    fill_in 'user-password', with: user_atts[:password] || 'goodpass'
    fill_in 'user-password-confirmation', with: user_atts[:password_confirmation] || 'goodpass'
    click_on 'Sign Up'
  end

  def local_signin(session_atts = {})
    logout
    visit login_path
    fill_in 'session[email]', with: session_atts[:email] || 'testuser@cisco.com'
    fill_in 'session[password]', with: session_atts[:password] || 'goodpass'
    click_on 'Sign In'
  end

  def logout
    Capybara.reset_sessions!
  end

  def confirm_email(token)
    visit email_confirmation_path(token)
  end

  private

  def unset_sso_domain
    ApiTestHelper.env = { 'SSO_DOMAIN' => '' }
    ENV['SSO_DOMAIN'] = nil
  end

  def set_sso_domain
    ApiTestHelper.env = { 'SSO_DOMAIN' => 'cisco.com' }
    ENV['SSO_DOMAIN'] = 'cisco.com'
  end
end
