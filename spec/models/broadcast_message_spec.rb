# frozen_string_literal: true

require 'rails_helper'

describe BroadcastMessage, type: :model do
  it 'deactivates all past broadcast messages if a new live message is created' do
    BroadcastMessage.create_and_activate(text: 'a broadcast message')

    expect(BroadcastMessage.first).to be_live

    BroadcastMessage.create_and_activate(text: 'a broadcast message')

    expect(BroadcastMessage.first).to_not be_live
    expect(BroadcastMessage.second).to be_live

    BroadcastMessage.create_and_activate(text: 'a broadcast message')

    expect(BroadcastMessage.first).to_not be_live
    expect(BroadcastMessage.second).to_not be_live
    expect(BroadcastMessage.last).to be_live
  end

  it 'has default values that make sense' do
    message = BroadcastMessage.create_and_activate(text: 'a broadcast message')

    expect(message).to be_live
    expect(message.severity).to eq('warning')
  end

  it 'validates "severity"' do
    message = build :broadcast_message
    message.severity = 'unsupported'
    expect(message).to_not be_valid
    expect(message.errors.full_messages).to eq(['Severity is not included in the list'])

    message.severity = 'danger'
    expect(message).to be_valid

    message.severity = 'info'
    expect(message).to be_valid

    message.severity = 'warning'
    expect(message).to be_valid

    message.severity = 'success'
    expect(message).to be_valid
  end

  describe '.turn_off' do
    it 'sets "live" to false for all broadcast messages' do
      BroadcastMessage.create_and_activate(text: 'a broadcast message')
      BroadcastMessage.create_and_activate(text: 'a broadcast message')
      expect(BroadcastMessage.where(live: true).count).to eq(1)

      BroadcastMessage.turn_off

      expect(BroadcastMessage.where(live: true).count).to eq(0)
    end
  end
end
