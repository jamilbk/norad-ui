export function findFirst (selector) {
  return this.findAll(selector).at(0)
}
