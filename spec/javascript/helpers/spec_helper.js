// Fail the whole suite on console.error
console.error = (message) => {
  throw new Error(message)
}
