import { createLocalVue, shallow } from '@vue/test-utils'
import Vuex from 'vuex'
import { findFirst } from 'helpers/find_first'
import axios from 'axios'

// storeGenerator is used to return a fresh store instance for each wrapper.
// Otherwise, the whole suite will share the same store, even if wrapper.destroy()
// is called.
import { storeGenerator } from 'store'

const localVue = createLocalVue()
localVue.use(Vuex)

// There's no good way to mock $http on the Vue component with vue-test-utils so
// we'll set it here and let moxios take over from here.
localVue.prototype.$http = axios
const wrapperFactory = (component, opts = {}) => {
  if (opts.validators)
    for (let v of opts.validators)
      localVue.use(v)

  let propsData = opts.propsData
  let store = opts.store || storeGenerator()
  let cloned = shallow(component, { mocks: opts.mocks, store, localVue, propsData })

  cloned.findFirst = findFirst

  return cloned
}

export {
  wrapperFactory
}
