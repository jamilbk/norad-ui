import StatusBadge from 'components/results/by_requirement/status_badge.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('StatusBadge', () => {
  let wrapper

  const props = {
    status: 'fail',
    type: undefined
  }

  beforeEach(() => {
    wrapper = wrapperFactory(StatusBadge, { propsData: props })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the props defined', () => {
    expect(wrapper.props()).toEqual(props)
  })

  describe('computes status_class', () => {
    it('for fail', () => {
      wrapper.setProps({ status: 'fail' })
      expect(wrapper.contains('.stats-badge-danger.fa-times-circle')).toBe(true)
    })

    it('for pass', () => {
      wrapper.setProps({ status: 'pass' })
      expect(wrapper.contains('.stats-badge-success.fa-check-circle')).toBe(true)
    })

    it('for error', () => {
      wrapper.setProps({ status: 'error' })
      expect(wrapper.contains('.stats-badge-warning.fa-exclamation-triangle')).toBe(true)
    })

    it('for warning', () => {
      wrapper.setProps({ status: 'warn' })
      expect(wrapper.contains('.stats-badge-warning.fa-exclamation-triangle')).toBe(true)
    })

    it('for info', () => {
      wrapper.setProps({ status: 'info' })
      expect(wrapper.contains('.stats-badge-info.fa-info-circle')).toBe(true)
    })

    it('for pending', () => {
      wrapper.setProps({ status: 'pending' })
      expect(wrapper.contains('.stats-badge-default.fa-clock-o')).toBe(true)
    })

    it('for machines', () => {
      wrapper.setProps({ type: 'machine' })
      expect(wrapper.contains('.fa-laptop')).toBe(true)
    })

    it('for services', () => {
      wrapper.setProps({ type: 'service' })
      expect(wrapper.contains('.fa-gears')).toBe(true)
    })

  })
})
