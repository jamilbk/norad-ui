import Expander from 'components/results/by_requirement/expander.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('Expander', () => {
  let wrapper

  const props = {
    show: false
  }

  beforeEach(() => {
    wrapper = wrapperFactory(Expander)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has props defined', () => {
    expect(wrapper.props()).toEqual(props)
  })

  it('computes icon_class', () => {
    expect(wrapper.vm.icon_class).toBe('fa-caret-right')
    wrapper.setProps({ show: true })
    expect(wrapper.vm.icon_class).toBe('fa-caret-down')
  })
})
