import MachineHeader from 'components/results/by_requirement/machine_header.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('MachineHeader', () => {
  let wrapper
  const props = {
    machine: {
      id: 1,
      overall_status: 'fail'
    },
    requirement_id: -1
  }

  beforeEach(() => {
    wrapper = wrapperFactory(MachineHeader, { propsData: props })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  describe('when computing machine_link', () => {
    it('should be the machine_path', () => {
      expect(wrapper.vm.machine_link).toBe(Routes.results_machine_path(1))
    })
  })

  it('has props defined', () => {
    expect(wrapper.props()).toEqual(props)
  })
})
