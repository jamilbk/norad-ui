import ResultRow from 'components/results/by_requirement/result_row.vue'
import ResultOutput from 'components/results/by_requirement/result_output.vue'
import ResultDescription from 'components/results/by_requirement/result_output.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ResultRow', () => {
  let wrapper
  let result = {
    signature: 'test signature',
    machine_id: 1
  }

  beforeEach(() => {
    wrapper = wrapperFactory(ResultRow, { propsData: { result }})
  })

  afterEach(() => {
    wrapper.destroy()
  })

  describe('when result is ignored', () => {
    describe('and show_ignored_results is true', () => {
      beforeEach(() => {
        wrapper.vm.$store.commit('results_by_requirement/ignoreResult', 'test signature')
        wrapper.vm.$store.dispatch('results_by_requirement/toggleIgnoredResults')
      })

      it('show_result should be true', () => {
        expect(wrapper.vm.show_ignored_results).toBe(true)
        expect(wrapper.vm.show_result).toBe(true)
      })
    })

    describe('and show_ignored_results is false', () => {
      beforeEach(() => {
        wrapper.vm.$store.commit('results_by_requirement/ignoreResult', 'test signature')
      })

      it('show_result should be false', () => {
        expect(wrapper.vm.ignored_result_signatures).toEqual(['test signature'])
        expect(wrapper.vm.show_ignored_results).toBe(false)
        expect(wrapper.vm.show_result).toBe(false)
      })
    })
  })

  describe('when result is not ignored', () => {
    describe('and show_ignored_results is true', () => {
      beforeEach(() => {
        wrapper.vm.$store.dispatch('results_by_requirement/toggleIgnoredResults')
      })

      it('show_result should be true', () => {
        expect(wrapper.vm.ignored_result_signatures).toEqual([])
        expect(wrapper.vm.show_ignored_results).toBe(true)
        expect(wrapper.vm.show_result).toBe(true)
      })
    })

    describe('and show_ignored_results is false', () => {
      it('show_result should be true', () => {
        expect(wrapper.vm.ignored_result_signatures).toEqual([])
        expect(wrapper.vm.show_ignored_results).toBe(false)
        expect(wrapper.vm.show_result).toBe(true)
      })
    })
  })

  describe('when clicking Result view buttons', () => {
    it('should toggle show_output', () => {
      // if a user clicks on the button text
      expect(wrapper.vm.show_output).toBe(false)
      wrapper.findAll('#toggle-output').at(0).trigger('click')
      expect(wrapper.vm.show_output).toBe(true)
      wrapper.findAll('#toggle-output').at(0).trigger('click')
      expect(wrapper.vm.show_output).toBe(false)

      // if a user clicks on the button icon
      wrapper.findAll('#toggle-output i').at(0).trigger('click')
      expect(wrapper.vm.show_output).toBe(true)
      wrapper.findAll('#toggle-output i').at(0).trigger('click')
      expect(wrapper.vm.show_output).toBe(false)
    })

    it('should toggle show_description', () => {
      // if a user clicks on the button text
      expect(wrapper.vm.show_description).toBe(false)
      wrapper.findAll('#toggle-description').at(0).trigger('click')
      expect(wrapper.vm.show_description).toBe(true)
      wrapper.findAll('#toggle-description').at(0).trigger('click')
      expect(wrapper.vm.show_description).toBe(false)

      // if a user clicks on the button icon
      wrapper.findAll('#toggle-description i').at(0).trigger('click')
      expect(wrapper.vm.show_description).toBe(true)
      wrapper.findAll('#toggle-description i').at(0).trigger('click')
      expect(wrapper.vm.show_description).toBe(false)
    })
  })
})
