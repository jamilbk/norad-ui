import SecurityTestHeader from 'components/results/by_requirement/security_test_header.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('SecurityTestHeader', () => {
  let wrapper
  const props = {
    security_test: {
      display_title: 'foobar_title'
    },
    requirement_id: -1
  }

  beforeEach(() => {
    wrapper = wrapperFactory(SecurityTestHeader, { propsData: props })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the props defined', () => {
    expect(wrapper.props()).toEqual(props)
  })

  it('sets the security test title', () => {
    expect(wrapper.text()).toBe('foobar_title')
  })
})
