import ServiceHeader from 'components/results/by_requirement/service_header.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ServiceHeader', () => {
  let wrapper
  const props = {
    service: {
      name: 'foobar'
    }
  }

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceHeader, { propsData: props })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has props defined', () => {
    expect(wrapper.props()).toEqual(props)
  })

  it('displays service name', () => {
    expect(wrapper.text()).toBe('foobar')
  })
})
