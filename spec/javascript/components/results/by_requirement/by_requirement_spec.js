import ResultsByRequirement from 'components/results/by_requirement/by_requirement.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ResultsByRequirement', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ResultsByRequirement)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('computes requirements_empty when reqs is null', () => {
    wrapper.vm.$store.commit('results_by_requirement/setRequirements', null)
    expect(wrapper.vm.requirements_empty).toBe(true)
  })

  it('computes requirements_empty when reqs is empty', () => {
    wrapper.vm.$store.commit('results_by_requirement/setRequirements', {})
    expect(wrapper.vm.requirements_empty).toBe(true)
  })

  it('computes requirements_empty when reqs is provided', () => {
    wrapper.vm.$store.commit('results_by_requirement/setRequirements', { foo: 'bar' })
    expect(wrapper.vm.requirements_empty).toBe(false)
  })

  it('has the mounted hook', () => {
    expect(typeof ResultsByRequirement.mounted).toBe('function')
  })

  it('has props defined', () => {
    let expected_keys = Object.keys(wrapper.props())
    expect(expected_keys).toEqual(['docker_command_id', 'org_id', 'export_queues_present'])
  })

  it('calls initData when mounted', () => {
    wrapper.setProps({ docker_command_id: 1, org_id: 2 })
    spyOn(wrapper.vm, 'initData')

    wrapper.vm.$mount()
    expect(wrapper.vm.initData).toHaveBeenCalledWith({
      docker_command_id: 1,
      org_id: 2,
      export_queues_present: false
    })
  })
})
