import Common from 'components/services/common.vue'

// This component is not rendered directly, but instead extended with other components.
describe('Common', () => {
  it('has the mounted hook', () => {
    expect(typeof Common.mounted).toBe('function')
  })

  it('has the setServiceProp method defined', () => {
    expect(typeof Common.methods.setServiceProp).toBe('function')
  })

  it('has the setPortDefaults method defined', () => {
    expect(typeof Common.methods.setPortDefaults).toBe('function')
  })
})
