import ServiceCreds from 'components/services/service_creds.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'
import ServiceValidator from 'validators/service'

describe('ServiceCreds', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceCreds, { validators: [ServiceValidator] })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('next_view is service_encryption_protocol', () => {
    expect(wrapper.vm.next_view).toEqual('service_encryption_protocol')
  })
})
