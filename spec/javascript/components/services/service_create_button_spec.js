import ServiceCreateButton from 'components/services/service_create_button.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ServiceCreateButton', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceCreateButton)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the create method', () => {
    expect(typeof wrapper.vm.create).toBe('function')
  })

  it('has the props defined', () => {
    expect(Object.keys(wrapper.props()).sort()).toEqual(['hidden', 'machine_id'])
  })
})
