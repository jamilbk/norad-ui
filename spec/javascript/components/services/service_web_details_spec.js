import ServiceWebDetails from 'components/services/service_web_details.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ServiceWebDetails', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceWebDetails)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('next_view is service_web_auth_method', () => {
    expect(wrapper.vm.next_view).toEqual('service_web_auth_method')
  })
})
