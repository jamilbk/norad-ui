import ServiceApplicationType from 'components/services/service_application_type.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ServiceApplicationType', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceApplicationType, {
      propsData: { application_type_options: [] }
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the setApplicationTypeChanged method', () => {
    expect(typeof wrapper.vm.setApplicationTypeChanged).toBe('function')
  })

  it('has the application_type_options props', () => {
    expect(Object.keys(wrapper.props())).toEqual(['application_type_options'])
  })

  it('next_view is service_create', () => {
    expect(wrapper.vm.next_view).toBe('service_create')
  })
})
