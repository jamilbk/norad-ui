import ServiceType from 'components/services/service_type.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ServiceType', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceType)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('next_view is service_ident_required by default', () => {
    expect(wrapper.vm.next_view).toBe('service_ident_required')
  })
})
