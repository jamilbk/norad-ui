import NextViewButton from 'components/services/next_view_button.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('NextViewButton', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(NextViewButton)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the triggerNextView method', () => {
    expect(typeof wrapper.vm.triggerNextView).toBe('function')
  })

  it('has the props defined', () => {
    expect(Object.keys(wrapper.props()).sort()).toEqual(['disabled', 'hidden'])
  })
})
