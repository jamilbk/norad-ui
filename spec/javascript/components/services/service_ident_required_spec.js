import ServiceIdentRequired from 'components/services/service_ident_required.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ServiceIdentRequired', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceIdentRequired)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('next_view is service_encryption_protocol by default', () => {
    expect(wrapper.vm.next_view).toEqual('service_encryption_protocol')
  })
})
