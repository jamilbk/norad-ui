import PrevViewButton from 'components/services/prev_view_button.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('PrevViewButton', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(PrevViewButton)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the prevView method', () => {
    expect(typeof wrapper.vm.transitionToPrevView).toBe('function')
  })

  it('has the props defined', () => {
    expect(Object.keys(wrapper.props())).toEqual(['hidden'])
  })
})
