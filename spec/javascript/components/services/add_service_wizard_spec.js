import AddServiceWizard from 'components/services/add_service_wizard.vue'
import ServiceType from 'components/services/service_details.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'
import ServiceValidator from 'validators/service'

describe('AddServiceWizard', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(AddServiceWizard, { validators: [ServiceValidator] })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the props defined', () => {
    expect(Object.keys(wrapper.props()).sort()).toEqual(['application_type_options', 'machine_id'])
  })

  it('has the triggerTransitionToNextView method defined', () => {
    expect(typeof wrapper.vm.triggerTransitionToNextView).toBe('function')
  })

  it('has the setValidator function defined', () => {
    expect(typeof wrapper.vm.setValidator).toBe('function')
  })

  it('has the validate function defined', () => {
    expect(typeof wrapper.vm.validate).toBe('function')
  })

  it('starts with the service_type view', () => {
    expect(wrapper.vm.current_view).toBe('service_type')
  })
})
