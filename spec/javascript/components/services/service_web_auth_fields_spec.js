import ServiceWebAuthFields from 'components/services/service_web_auth_fields.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'
import ServiceValidator from 'validators/service'

describe('ServiceWebAuthFields', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceWebAuthFields, { validators: [ServiceValidator] })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('next_view is service_creds', () => {
    expect(wrapper.vm.next_view).toEqual('service_creds')
  })
})
