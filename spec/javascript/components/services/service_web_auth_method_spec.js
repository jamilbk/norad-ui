import ServiceWebAuthMethod from 'components/services/service_web_auth_method.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'
import ServiceValidator from 'validators/service'

describe('ServiceWebAuthMethod', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceWebAuthMethod, { validators: [ServiceValidator] })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('next_view is service_creds by default', () => {
    expect(wrapper.vm.next_view).toEqual('service_creds')
  })
})
