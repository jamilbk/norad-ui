import ServiceCreate from 'components/services/service_create.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ServiceCreate', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceCreate, {
      propsData: { application_type_options: [] }
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the mounted hook', () => {
    expect(typeof ServiceCreate.mounted).toBe('function')
  })

  it('application_protocol is null by default', () => {
    expect(wrapper.vm.application_protocol).toBe(null)
  })

  it('has the service computed property', () => {
    expect(wrapper.vm.service.name).toBe(null)
  })
})
