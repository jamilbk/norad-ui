import ServiceDetails from 'components/services/service_details.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'
import ServiceValidator from 'validators/service'

describe('ServiceDetails', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceDetails, { validators: [ServiceValidator] })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the mounted hook', () => {
    expect(typeof ServiceDetails.mounted).toBe('function')
  })

  it('next_view is service_create by default', () => {
    expect(wrapper.vm.next_view).toBe('service_create')
  })

  it('show_transport_protocol is false by default', () => {
    expect(wrapper.vm.show_transport_protocol).toBe(false)
  })

  it('has the setPortNumberChanged function', () => {
    expect(typeof wrapper.vm.setPortNumberChanged).toBe('function')
  })
})
