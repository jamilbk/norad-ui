import ServiceEncryptionProtocol from 'components/services/service_encryption_protocol.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('ServiceEncryptionProtocol', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ServiceEncryptionProtocol)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('next_view is service_details', () => {
    expect(wrapper.vm.next_view).toEqual('service_details')
  })
})
