import MachineModal from 'components/machines/machine-modal.vue'
import MachinesValidator from 'validators/machines'
import { wrapperFactory } from 'helpers/wrapper_factory'
import moxios from 'moxios'

describe('MachineModal', () => {
  let wrapper
  let el = (selector) => { return wrapper.findFirst(selector) }

  beforeEach(() => {
    wrapper = wrapperFactory(MachineModal, { propsData: { mode: 'new' }, validators: [MachinesValidator] })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('computes modalTitle', () => {
    expect(el('#modal-title').text()).toBe('New Machine')

    // XXX: For some odd reason that makes no sense (probably vue-test-utils bug), calling
    // wrapper.setProps() here doesn't update the template like it should, so we have to
    // create a new wrapper instance here which has the template freshly rendered.
    wrapper = wrapperFactory(MachineModal, { propsData: { mode: 'edit' }, validators: [MachinesValidator] })
    expect(wrapper.findFirst('.modal-header span').text()).toBe('Edit Machine')
  })

  it('computes errorModalTitle', () => {
    wrapper.setData({ ajaxErrors: ['ajax error'] })
    expect(el('#error-modal-title').text()).toBe('Unable to create machine')

    wrapper.setProps({ mode: 'edit' })
    wrapper.setData({ ajaxErrors: ['ajax error'] })
    expect(el('#error-modal-title').text()).toBe('Unable to update machine')
  })

  it('computes submitMachineButtonText', () => {
    expect(el('#submit-btn').text()).toBe('Create')

    // XXX: For some odd reason that makes no sense (probably vue-test-utils bug), calling
    // wrapper.setProps() here doesn't update the template like it should, so we have to
    // create a new wrapper instance here which has the template freshly rendered.
    wrapper = wrapperFactory(MachineModal, { propsData: { mode: 'edit' }, validators: [MachinesValidator] })
    expect(el('#submit-btn').text()).toBe('Save')
  })

  it('computes modalId', () => {
    expect(el('.modal').attributes().id).toBe('new-machine-modal')

    wrapper.setProps({ mode: 'edit' })
    expect(el('.modal').attributes().id).toBe('edit-machine-modal')
  })

  it('triggers submitMachine', () => {
    spyOn(wrapper.vm, 'createMachine')
    el('#submit-btn').trigger('click')
    expect(wrapper.vm.createMachine).toHaveBeenCalled()

    wrapper.setProps({ mode: 'edit' })
    spyOn(wrapper.vm, 'updateMachine')
    el('#submit-btn').trigger('click')
    expect(wrapper.vm.updateMachine).toHaveBeenCalled()
  })

  describe('mocked requests', () => {
    beforeEach(() => {
      moxios.install()
    })

    afterEach(() => {
      moxios.uninstall()
    })

    it('navigates to machine services after created', (done) => {
      spyOn(Routes, 'machine_services_path').and.callThrough()
      spyOn(window, 'redirectTo')
      wrapper.vm.$store.dispatch('machines/setOrganization', { organization:  { id: 4 }})
      wrapper.vm.createMachine()
      moxios.wait(() => {
        let req = moxios.requests.mostRecent()
        if (!req) throw 'CALLED FROM MACHINE MODAL'
        req.respondWith({
          status: 200,
          response: {
            machine: {
              id: 1
            }
          }
        }).then((what) => {
          expect(Routes.machine_services_path).toHaveBeenCalledWith({ id: 1 })
          expect(window.redirectTo).toHaveBeenCalledWith('/machines/1/services?machine_created=true')
          done()
        })
      })
    })
  })
})
