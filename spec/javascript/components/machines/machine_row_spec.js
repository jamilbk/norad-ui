import moxios from 'moxios'
import MachineRow from 'components/machines/machine-row.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

const machineFixture = (opts = {}) => {
  return {
    machine: {
      status: 'fail',
      name: 'foobar',
      id: 1,
      fqdn: 'machine.corp',
      ip: '0.0.0.0',
      use_fqdn_as_target: false,
      scheduled_service_discovery: null,
      ...opts
    }
  }
}

describe('MachineRow', () => {
  let wrapper
  const el = (selector) => { return wrapper.findFirst(selector) }

  beforeEach(() => {
    moxios.install()
    wrapper = wrapperFactory(MachineRow, { propsData: machineFixture({ status: 'fail' }) })
  })

  afterEach(() => {
    moxios.uninstall()
    wrapper.destroy()
  })

  it('computes discoveryActionState', () => {
    expect(el('#discover-service-btn').attributes().title).toBe('Schedule Service Discovery')
    wrapper.setProps(machineFixture({ scheduled_service_discovery: true }))
    wrapper.setProps(machineFixture({ scheduled_service_discovery: true }))
    expect(el('#discover-service-btn').attributes().title).toBe('Discovery in Progress')
  })

  it('computes statusColor', () => {
    wrapper.setProps(machineFixture({ status: 'pass' }))
    wrapper.setProps(machineFixture({ status: 'pass' }))
    expect(el('.machine-status').classes()).toContain('label-success')
    expect(el('.machine-status').text()).toBe('Pass')

    wrapper.setProps(machineFixture({ status: 'warn' }))
    expect(el('.machine-status').classes()).toContain('label-warning')
    expect(el('.machine-status').text()).toBe('Warn')

    wrapper.setProps(machineFixture({ status: 'error' }))
    expect(el('.machine-status').classes()).toContain('label-warning')
    expect(el('.machine-status').text()).toBe('Error')

    wrapper.setProps(machineFixture({ status: 'fail' }))
    expect(el('.machine-status').classes()).toContain('label-danger')
    expect(el('.machine-status').text()).toBe('Fail')

    wrapper.setProps(machineFixture({ status: 'info' }))
    expect(el('.machine-status').classes()).toContain('label-info')
    expect(el('.machine-status').text()).toBe('Info')

    wrapper.setProps(machineFixture({ status: 'unknown' }))
    expect(el('.machine-status').classes()).toContain('label-default')
    expect(el('.machine-status').text()).toBe('Unknown')
  })

  it('computes settingsMachinePath', () => {
    expect(el('#machine-settings-btn').attributes().href).toBe('/machines/1/settings')
  })

  it('computes machineServicesPath', () => {
    expect(el('#machine-services-btn').attributes().href).toBe('/machines/1/services')
  })

  it('computes machinePath', () => {
    expect(el('.machine-name').attributes().href).toBe('/machines/1')
  })

  it('computes truncatedName', () => {
    expect(el('.machine-name').text()).toBe(wrapper.vm.machine.name)
    wrapper.setProps(machineFixture({ name: 'This is a machine name that happens to be longer than 26 characters.' }))
    wrapper.setProps(machineFixture({ name: 'This is a machine name that happens to be longer than 26 characters.' }))
    expect(el('.machine-name').text()).toBe('This is a machine name tha...')
  })

  it('computes truncatedDescription', () => {
    wrapper.setProps(machineFixture({ description: 'desc' }))
    wrapper.setProps(machineFixture({ description: 'desc' }))
    expect(el('#machine-description').text()).toBe(wrapper.vm.machine.description)

    wrapper.setProps(machineFixture({
      description: 'This is a machine description that happens to be longer than 26 characters.'
    }))
    wrapper.setProps(machineFixture({
      description: 'This is a machine description that happens to be longer than 26 characters.'
    }))
    expect(el('#machine-description').text()).toBe(
      'This is a machine description that happens to be l...'
    )
  })

  it('computes targetAddress', () => {
    expect(el('.machine-scan-target').text()).toBe('0.0.0.0')
    wrapper.setProps(machineFixture({ use_fqdn_as_target: true }))
    wrapper.setProps(machineFixture({ use_fqdn_as_target: true }))
    expect(el('.machine-scan-target').text()).toBe('machine.corp')
  })

  it('can delete machines', (done) => {
    spyOn(wrapper.vm, 'refreshOrganizationData')
    spyOn(wrapper.vm, 'removeMachine')
    spyOn(window, 'confirm').and.returnValue(true)

    el('.machine-actions a[title="Delete Machine"]').trigger('click')

    moxios.wait(() => {
      let req = moxios.requests.mostRecent()
      req.respondWith({
        status: 200,
        response: {
          machine: {
            id: 1
          }
        }
      }).then((foobar) => {
        expect(wrapper.vm.refreshOrganizationData).toHaveBeenCalled()
        expect(wrapper.vm.removeMachine).toHaveBeenCalled()
        done()
      })
    })
  })
})
