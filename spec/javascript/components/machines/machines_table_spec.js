import Vue from 'vue'
import MachinesTable from 'components/machines/machines-table.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('MachinesTable', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(MachinesTable, {
      getters: {
        organization () {
          return {
            machine_count: 10,
            machine_assessment_summary: {
              passing: 1,
              erroring: 2,
              warning: 3,
              failing: 4
            },
            contains_enabled_tests: false,
            uid: 'mytest',
            id: 1,
            slug: 'mytest'
          }
        }
      },
      propsData: {
        machines: [{
          name: 'test-machine',
          id: 0,
          use_fqdn_as_target: true,
          fqdn: 'foobar',
          ip: '0.0.0.0'
        }],
        sortOrder: 'asc',
        sortColumn: 'name'
      }
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('computes sortMethod', () => {
    expect(wrapper.vm.sortMethod(wrapper.vm.machines[0])).toBe('test-machine')
    wrapper.setData({ sortColumn: 'scan_target' })
    expect(wrapper.vm.sortMethod(wrapper.vm.machines[0])).toBe('foobar')
  })

  it('computes filteredMachines', () => {
    wrapper.setProps({ machines: [{ name: 'c' }, { name: 'b' }, { name: 'a' }]})
    expect(wrapper.vm.filteredMachines).toEqual([
      { name: 'a' },
      { name: 'b' },
      { name: 'c' }
    ])

    wrapper.setProps({ machines: [{ name: 'a' }, { name: 'b' }, { name: 'c' }]})
    wrapper.setData({ sortOrder: 'desc' })
    expect(wrapper.vm.filteredMachines).toEqual([
      { name: 'c' },
      { name: 'b' },
      { name: 'a' }
    ])
  })

  it('updates and and reverse sorts by "status" properly', () => {
    wrapper.findFirst('#sort-by-status').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('status')
    expect(wrapper.vm.sortOrder).toBe('asc')

    wrapper.findFirst('#sort-by-status').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('status')
    expect(wrapper.vm.sortOrder).toBe('desc')

    // test sort when icon is clicked
    wrapper.findFirst('#sort-by-status i').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('status')
    expect(wrapper.vm.sortOrder).toBe('asc')

    wrapper.findFirst('#sort-by-status i').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('status')
    expect(wrapper.vm.sortOrder).toBe('desc')
  })

  it('updates and and reverse sorts by "scan_target" properly', () => {
    wrapper.findFirst('#sort-by-scan-target').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('scan_target')
    expect(wrapper.vm.sortOrder).toBe('asc')

    wrapper.findFirst('#sort-by-scan-target').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('scan_target')
    expect(wrapper.vm.sortOrder).toBe('desc')

    // test sort when icon is clicked
    wrapper.findFirst('#sort-by-scan-target i').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('scan_target')
    expect(wrapper.vm.sortOrder).toBe('asc')

    wrapper.findFirst('#sort-by-scan-target i').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('scan_target')
    expect(wrapper.vm.sortOrder).toBe('desc')
  })

  it('updates and and reverse sorts by "name" properly', () => {
    wrapper.findFirst('#sort-by-name').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('name')
    expect(wrapper.vm.sortOrder).toBe('desc')

    wrapper.findFirst('#sort-by-name').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('name')
    expect(wrapper.vm.sortOrder).toBe('asc')

    // test sort when icon is clicked
    wrapper.findFirst('#sort-by-name i').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('name')
    expect(wrapper.vm.sortOrder).toBe('desc')

    wrapper.findFirst('#sort-by-name i').trigger('click')
    expect(wrapper.vm.sortColumn).toBe('name')
    expect(wrapper.vm.sortOrder).toBe('asc')
  })
})
