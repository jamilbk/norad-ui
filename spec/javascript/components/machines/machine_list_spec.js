import MachineList from 'components/machines/machine-list.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'
import moxios from 'moxios'

describe('MachineList', () => {
  let wrapper
  const machines = [{ status: 'Warn' }]

  beforeEach((done) => {
    moxios.install()
    wrapper = wrapperFactory(MachineList, { propsData: { organizationId: 'test' } })

    // The MachineList component fetches data on the create() lifecycle
    // hook so we need to catch and mock this here
    moxios.wait(() => {
      let req = moxios.requests.mostRecent()
      if (!req) throw 'CALLED FROM MACHINE LIST'
      req.respondWith({
        status: 200,
        response: {
          machines: [{ status: 'Warn' }]
        }
      }).then(() => { done() })
    }, 10)
  })

  afterEach(() => {
    moxios.uninstall()
    wrapper.destroy()
  })

  it('allows filtering by status', () => {
    wrapper.setData({ pageLoading: false, statuses: [], filterKey: { value: 'Warn' }})
    expect(wrapper.vm.filteredMachines).toEqual(machines)

    wrapper.setData({ pageLoading: false, statuses: [], filterKey: { value: 'Fail' }})
    expect(wrapper.vm.filteredMachines).toEqual([])
  })
})
