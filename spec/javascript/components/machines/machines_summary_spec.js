import Vue from 'vue'
import { storeGenerator } from 'store'
import MachinesSummary from 'components/machines/machines-summary.vue'
import { wrapperFactory } from 'helpers/wrapper_factory'

describe('MachinesSummary', () => {
  let wrapper

  beforeEach(() => {
    let store = storeGenerator()
    store.dispatch('machines/setOrganization', {
      organization:
        {
          machine_count: 10,
          machine_assessment_summary: {
            passing: 1,
            erroring: 2,
            warning: 3,
            failing: 4
          },
          contains_enabled_tests: false,
          uid: 'mytest',
          id: 1,
          slug: 'mytest'
        }
      })
    wrapper = wrapperFactory(MachinesSummary, { store })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('shows the machine count', () => {
    expect(wrapper.vm.organization.id).toBe(1)
    expect(wrapper.findFirst('#machine-count').text()).toEqual('10')
  })

  it('shows the machine assessment summary passing count', () => {
    expect(wrapper.findFirst('#machine-passing-count').text()).toEqual('1')
  })

  it('shows the machine assessment summary erroring and warning count', () => {
    expect(wrapper.findFirst('#machine-erroring-and-warning-count').text()).toEqual('5')
  })

  it('shows the machine assessment summary failing count', () => {
    expect(wrapper.findFirst('#machine-failing').text()).toEqual('4')
  })
})
