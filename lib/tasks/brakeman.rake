# frozen_string_literal: true

namespace :brakeman do
  desc 'Run Brakeman'
  task :run do |_t, _args|
    require 'brakeman'
    run_brakeman
  end

  def run_brakeman
    return true unless ENV['RUN_BRAKEMAN']
    tracker = Brakeman.run app_path: '.', quiet: false

    puts tracker.report
    if !tracker.filtered_warnings.empty?
      tracker.filtered_warnings.each { |w| puts w } && raise('New Brakeman vulnerabilities found.')
    else
      puts 'No new vulnerabilities found by Brakeman.'
    end
  end
end
