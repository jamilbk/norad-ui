# frozen_string_literal: true

# Prevent the old js:routes from firing
Rake::Task['js:routes'].clear

namespace :js do
  desc 'Make a js file that will have functions that will return restful routes/urls.'
  task :routes, [:filename] => [:environment] do |_, args|
    require 'js-routes'
    JsRoutes.generate!(args[:filename])
  end
end
