# rubocop:disable all
module RemoteIpLogger
  extend ActiveSupport::Concern

  included do
    define_method :started_request_message do |request|
      'Started %s "%s" for %s at %s' % [
        request.request_method,
        request.filtered_path,
        request.remote_ip,
        Time.now.to_default_s ]
    end

    protected :started_request_message
  end
end

Rails::Rack::Logger.send(:include, RemoteIpLogger)
# rubocop:enable all
