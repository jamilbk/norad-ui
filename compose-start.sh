#!/bin/bash

# Source private ENV vars

for f in $(ls /norad/env); do
  source /norad/env/$f
done

# This is a helper script you can use to supervise unicorn,
# it allows you to perform a live restart by sending it a USR2 signal

if [ -z "$PRODUCTION_DEBUG" ]; then
  exec bundle exec unicorn -E production -c $1
fi

function on_exit()
{
  kill $UNICORN_PID
  echo "Shutting Down Unicorn"
}

function on_reload()
{
  echo "Reloading unicorn"
  kill -s USR2 $UNICORN_PID
  sleep 10
  NEW_UNICORN_PID=`cat /path/to/unicorn.pid`
  echo "Old pid is: $UNICORN_PID New pid is: $NEW_UNICORN_PID"
  UNICORN_PID=$NEW_UNICORN_PID
}

export UNICORN_SUPERVISOR_PID=$$

trap on_exit EXIT
trap on_reload USR2

bundle exec unicorn -E production -c $1 &
UNICORN_PID=$!

echo "supervisor pid: $UNICORN_SUPERVISOR_PID unicorn pid: $UNICORN_PID"

while [ -e /proc/$UNICORN_PID ]
do
  sleep 0.1
done
