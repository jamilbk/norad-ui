# frozen_string_literal: true

json.organization do
  json.id @org.id
  json.uid @org.uid
  json.slug @org.slug
  json.token @org.token
  json.machine_count @org.machine_count
  json.machine_assessment_summary @org.machine_assessment_summary
  json.contains_enabled_tests @org.contains_enabled_tests
end
