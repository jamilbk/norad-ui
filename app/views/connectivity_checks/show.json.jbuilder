# frozen_string_literal: true

json.call(@check, :status, :status_reason, :updated_at)
