# frozen_string_literal: true

json.organization do
  json.id @org.id
  json.uid @org.uid
  json.slug @org.slug
  json.token @org.token
  json.machine_count @org.machine_count
  json.machine_assessment_summary @org.machine_assessment_summary
  json.contains_enabled_tests @org.contains_enabled_tests
end

json.machines @org.machines do |machine|
  json.id machine.id
  json.ip machine.ip
  json.name machine.name
  json.description machine.description
  json.fqdn machine.fqdn
  json.status machine.status
  json.use_fqdn_as_target machine.use_fqdn_as_target
  json.scheduled_service_discovery machine.scheduled_service_discovery
  json.has_services machine.services.present?
end
