import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import machines from './modules/machines'
import services from './modules/services'
import results_by_requirement from './modules/results_by_requirement'

Vue.use(Vuex)

// Allows the ability to create multiple separate store instances
const storeGenerator = () => {
  let opts = {
    state: {},
    strict: true,
    modules: {
      machines,
      services,
      results_by_requirement
    },
    actions: {},
    mutations: {},
    getters: {}
  }

  return new Vuex.Store(_.cloneDeep(opts))
}

export default storeGenerator()
export { storeGenerator }
