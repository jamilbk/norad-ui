export default {
  setMachines (context, obj) {
    context.commit('setMachines', obj)
  },

  setActiveEditedMachine (context, obj) {
    context.commit('setActiveEditedMachine', obj)
  },

  setOrganization (context, obj) {
    context.commit('setOrganization', obj)
  },

  removeMachine (context, obj) {
    context.commit('removeMachine', obj)
  },

  updateMachineAttributes (context, obj) {
    context.commit('updateMachineAttributes', obj)
  }
}
