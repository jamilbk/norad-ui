import actions from './actions'
import getters from './getters'
import mutations from './mutations'

export default {
  state: {
    dataInitialized: false,
    requirements: null,
    show_ignored_results: false,
    org_id: null,
    docker_command_id: null,
    ignored_result_signatures: [],
    exported_result_ids: [],
    export_queues_present: null
  },
  actions,
  getters,
  mutations,
  namespaced: true
}
