export default {
  dataInitialized (state) {
    state.dataInitialized = true
  },

  setOrgId (state, org_id) {
    state.org_id = org_id
  },

  setExportQueuesPresent (state, export_queues_present) {
    state.export_queues_present = export_queues_present
  },

  setDockerCommandId (state, docker_command_id) {
    state.docker_command_id = docker_command_id
  },

  setRequirements (state, requirements) {
    state.requirements = requirements
  },

  toggleIgnoredResults (state, boolean) {
    state.show_ignored_results = boolean
  },

  ignoreResult (state, result_signature) {
    if (!state.ignored_result_signatures.includes(result_signature))
      state.ignored_result_signatures.push(result_signature)
  },

  exportResult (state, result_id) {
    if (!state.exported_result_ids.includes(result_id)) state.exported_result_ids.push(result_id)
  },

  stopIgnoring (state, result_signature) {
    let index = state.ignored_result_signatures.indexOf(result_signature)
    if (state.ignored_result_signatures.includes(result_signature)) state.ignored_result_signatures.splice(index, 1)
  },

  incrementIgnoringCount (state, requirement_id) {
    state.requirements[requirement_id].ignoring_count += 1
  },

  decrementIgnoringCount (state, requirement_id) {
    state.requirements[requirement_id].ignoring_count -= 1
  }
}
