export default {
  transitionToNextView (state) {
    state.view_history.push(state.current_view)
    state.current_view = state.next_view
  },
  transitionToPrevView (state) {
    state.next_view = state.current_view
    state.current_view = state.view_history.pop()
  },
  setNextView (state, view) {
    state.next_view = view
  },

  // If a user enters their own custom port number, don't override it with defaults
  // when the service type or encryption type changes.
  setPortNumberChanged (state) {
    state.port_number_changed = true
  },
  setApplicationTypeChanged (state) {
    state.application_type_changed = true
  },
  setCreationErrorString (state, error_string) {
    state.creation_error_string = error_string
  },
  setFinalServiceObj (state, service_obj) {
    state.final_service_obj = service_obj
  }
}
