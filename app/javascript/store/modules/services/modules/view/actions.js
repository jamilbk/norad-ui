export default {
  setNextView (context, next_view) {
    context.commit('setNextView', next_view)
  },

  transitionToNextView (context) {
    context.commit('transitionToNextView')
  },

  transitionToPrevView (context) {
    context.commit('transitionToPrevView')
  },

  setApplicationTypeChanged (context) {
    context.commit('setApplicationTypeChanged')
  },

  setCreationErrorString (context, error_string) {
    context.commit('setCreationErrorString', error_string)
  },

  setFinalServiceObj (context, service_obj) {
    context.commit('setFinalServiceObj', service_obj)
  },

  setPortNumberChanged (context) {
    context.commit('setPortNumberChanged')
  }
}
