import service from './modules/service'
import view from './modules/view'

export default {
  namespaced: true,
  modules: {
    service,
    view
  }
}
