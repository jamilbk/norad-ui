const initial_set = function(data_key, value) {
  $(document).data(data_key, value);
};

// Methods related to transport protocol

const change_port_type = function(e) {
  set_service_names();
  set_default_service_name();
};

const protocol_value = function() {
  if ($('#port-type').val().length) { return $('#port-type').val(); } else { return 'tcp'; }
};

// Methods related to port number

const change_port_number = function(e) {
  if (is_service_type_frozen()) {
    if (service_type_value() !== 'GenericService') { return; }
    set_default_service_name();
    return;
  }
  $('#encryption-menu').prop('disabled', '');
  handle_application_type_states();
  handle_ssh_port();
  handle_http_port();
  handle_https_port();
  if (!is_special_port()) {
    set_generic();
    set_default_service_name();
    set_service_type_options(service_type_value());
    return;
  }
};

var is_special_port = function() {
  if ((port_number() === '22') || (port_number() === '80') || (port_number() === '443')) {
    return true;
  }
  return false;
};

var port_number = () => $('#port-number').val();

const initial_port_number = () => $(document).data('initial_port');

const set_default_port_number = function() {
  $('#port-number').val(initial_port_number);
};

var handle_ssh_port = function() {
  if (port_number() === '22') {
    set_ssh();
  }
};

var handle_http_port = function() {
  if (port_number() === '80') {
    set_http();
    set_encryption_value('cleartext');
  }
};

var handle_https_port = function() {
  if (port_number() === '443') {
    set_http();
    set_encryption_value('ssl');
  }
};

// Methods related to service type

const change_service_type = function(e) {
  const service_type = $(e.target).val();
  set_service_type_options(service_type);
};

var service_type_value = () => $('#service-type').val();

var is_service_type_frozen = () => $(document).data('is_service_type_frozen');

var handle_application_type_states = function() {
  $('#application-name').prop('disabled', '');
  if (!is_service_type_frozen()) {
    $('#service-type').prop('disabled', '');
  }
};

var set_ssh = function() {
  unrestrict_encryption_types();
  set_encryption_value('ssh');
  $('select#encryption-menu').prop('disabled', 'disabled');
  $('.web-app-config-section').addClass('hidden');
  $('.service-identity-section').addClass('hidden');
  set_application_type(22, protocol_value());
  $('#application-name').prop('disabled', 'disabled');
  $('#service-type').val('SshService');
};

var set_http = function() {
  $('select#encryption-menu').prop('disabled', '');
  $('.web-app-config-section').removeClass('hidden');
  $('.service-identity-section').removeClass('hidden');
  restrict_encryption_types();
  set_application_type(80, protocol_value());
  $('#application-name').prop('disabled', 'disabled');
  $('#service-type').val('WebApplicationService');
};

var set_generic = function() {
  unrestrict_encryption_types();
  $('select#encryption-menu').prop('disabled', '');
  $('#application-name').prop('disabled', '');
  $('select#encryption-menu').val(encryption_value());
  $('.web-app-config-section').addClass('hidden');
  $('.service-identity-section').removeClass('hidden');
  $('#service-type').val('GenericService');
  set_default_service_name();
};

var set_service_type_options = function(service_type) {
  if (service_type === 'WebApplicationService') {
    set_http();
  } else if (service_type === 'SshService') {
    set_ssh();
  } else {
    set_generic();
  }
};

// Methods related to encryption type

const change_encryption_menu = function(e) {
  if ((encryption_value() === 'cleartext') || (encryption_value() === 'ssl')) {
    if (service_name() === 'ssh') {
      set_application_type(80, protocol_value());
    }
  } else if (encryption_value() === 'ssh') {
    if (service_name() === 'http') {
      set_application_type(22, protocol_value());
    }
  }
};

var encryption_value = function() {
  if ($('#encryption-menu').val().length) { return $('#encryption-menu').val(); } else { return 'cleartext'; }
};

var set_encryption_value = function(encryption_val) {
  $('select#encryption-menu').val(encryption_val);
};

var restrict_encryption_types = function() {
  const options = $(document).data('encryption-options').filter('[value!=ssh]');
  if (encryption_value() === 'cleartext') {
    $('#encryption-menu').html(options);
    set_encryption_value('cleartext');
  } else {
    $('#encryption-menu').html(options);
    set_encryption_value('ssl');
  }
};

var unrestrict_encryption_types = function() {
  const options = $(document).data('encryption-options');
  if (encryption_value() === 'cleartext') {
    $('#encryption-menu').html(options);
    set_encryption_value('cleartext');
  } else if (encryption_value() === 'ssl') {
    $('#encryption-menu').html(options);
    set_encryption_value('ssl');
  } else {
    $('#encryption-menu').html(options);
    set_encryption_value('ssh');
  }
};

// Methods related to service name

const change_application_name = function(e) {
  if (e.target.selectedOptions.length === 2) {
    if (e.target.selectedOptions[0] === previously_selected_option()) {
      e.target.selectedOptions[0].selected = false;
    } else if (e.target.selectedOptions[1] === previously_selected_option()) {
      e.target.selectedOptions[1].selected = false;
    }
  }
  $(document).data('application-name-selection', e.target.selectedOptions[0]);
  set_application_type_info();
  if ((service_name() === 'ssh') && (encryption_value() !== 'ssh')) {
    set_encryption_value('ssh');
  } else if ((service_name() === 'http') && (encryption_value() === 'ssh')) {
    set_encryption_value('ssl');
  }
};

var service_name = () => $('#application-name').val()[0];

var set_service_names = function() {
  const options = $(document).data('sn-options').filter(`[data-protocol=${protocol_value()}]`);
  $('#application-name').html(options);
};

var previously_selected_option = () => $(document).data('application-name-selection');

var set_default_service_name = function() {
  const port = $('#port-number').val();
  if (port === '') {
    $('#application-name').select2({'width': '100%'});
    return;
  }
  let default_value = undefined;
  if ((service_type_value() === 'GenericService') || is_special_port()) {
    const new_port = port === '443' ? '80' : port;
    const options = $(document).data('sn-options').filter(`[data-port=${new_port}]`).filter(`[data-protocol=${protocol_value()}]`);
    if (options.length) {
      default_value = options.first();
    }
  } else if (service_type_value() === 'SshService') {
    default_value = $(`#application-name option[data-port=22][data-protocol=${protocol_value()}]`).first();
  } else if (service_type_value() === 'WebApplicationService') {
    default_value = $(`#application-name option[data-port=80][data-protocol=${protocol_value()}]`).first();
  }
  if (typeof default_value !== 'undefined') {
    if (default_value.length === 1) {
      $('#application-name').val([]);
      default_value.prop('selected', 'selected');
      $('#application-name').trigger('change');
      set_application_type_info();
    }
  }
  $('#application-name').select2({'width': '100%'});
};

var set_application_type = function(port, protocol) {
  if ((port === null) || (protocol === null) || (port === '') || (protocol === '')) { return; }
  const new_port = port === '443' ? '80' : port;
  const selected_value = $(`#application-name option[data-port=${new_port}][data-protocol=${protocol}]`).first();
  selected_value.prop('selected', 'selected');
  $('#application-name').trigger('change');
  set_application_type_info();
  $('#application-name').select2({'width': '100%'});
};

var set_application_type_info = function() {
  const application_port = $('#application-name option:selected').attr('data-port');
  const application_protocol = $('#application-name option:selected').attr('data-protocol');
  $('#application-port').val(application_port);
  $('#application-protocol').val(application_protocol);
};

// Set initial defaults

const set_initial_form_defaults = function(freeze_service_type_value) {
  $('#service-type').prop('disabled', 'disabled');
  if (freeze_service_type_value) {
    $('#application-type').prop('disabled', 'disabled');
    if (service_type_value() === 'SshService') {
      set_ssh();
    }
    if (service_type_value() === 'WebApplicationService') {
      set_http();
    }
  } else {
    $('#application-name').prop('disabled', 'disabled');
  }
};

// Save callback

const save_callback = function(e) {
  e.preventDefault();
  if ((service_name() === 'http') && (encryption_value() === 'ssl')) {
    $('#application-name').val('https');
    $('#application-port').val('443');
  }
  $('#service-form').submit();
};

// Set callbacks on the actual form

const set_service_form_callbacks = function(service_type, freeze_service_type, initial_port, port, protocol) {
  $('#port-type').on('change', change_port_type);
  $('#port-number').on('change', change_port_number);
  $('#service-type').on('change', change_service_type);
  $('#encryption-menu').on('change', change_encryption_menu);
  $('#application-name').on('change', change_application_name);
  $('button#save-service').on('click', save_callback);

  $('#service-form-modal').modal('show');

  $(function() {
    initial_set('sn-options', $('#application-name').children().clone());
    initial_set('encryption-options', $('#encryption-menu').children().clone());
    initial_set('application-name-selection', $('#application-name').children().filter('[selected=true]'));
    initial_set('is_service_type_frozen', freeze_service_type);
    initial_set('initial_port', initial_port);
    set_service_names();
    set_default_service_name();
    set_service_type_options(service_type);
    set_application_type(port, protocol);
    set_initial_form_defaults(freeze_service_type);
    if ($('#default_service_type').val() == 'ssh') {
      $('[name="service[port]"]').val('22');
      $('[name="service[port]"]').trigger('change');
    }
  });
};

// This event listener is triggered from edit.js.haml and new.js.haml.
$('body').on('upsert_form', function(e) {
  set_service_form_callbacks(e.service_type, e.freeze_service_type, e.service_port, e.port, e.protocol);
});
