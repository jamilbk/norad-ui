const maximum_length_of_machine_name = 36;

const validate_machine_name_on_paste = function(e) {
  const pasted_text = e.originalEvent.clipboardData.getData('Text');
  const original_value = $('#machine-name .form-control').val();
  if ((pasted_text.length + original_value.length) > maximum_length_of_machine_name) {
    return display_error();
  }
};

const validate_machine_name_on_keydown = function(e) {
  const input = $('#machine-name .form-control').val();
  if (e.key === undefined) {
    // This is required for browsers that do not support e.key
    if ((input.length === maximum_length_of_machine_name) && valid_key(e.keyCode)) {
      return display_error();
    }
  } else {
    // This variant is more reliable for browsers that support e.key
    if ((input.length === maximum_length_of_machine_name) && (e.key.length === 1)) {
      return display_error();
    }
  }
};

var valid_key = function(keycode) {
  if ((keycode > 47) && (keycode < 58)) { return true; }   // number keys
  if (keycode === 32) { return true; }                    // spacebar
  if ((keycode > 64) && (keycode < 91)) { return true; }   // letter keys
  if ((keycode > 95) && (keycode < 112)) { return true; }  // numpad keys
  if ((keycode > 185) && (keycode < 193)) { return true; } // ;=,-./`
  if ((keycode > 218) && (keycode < 223)) { return true; } // [\]'
  return false;
};

const validate_machine_name_on_keyup = function(e) {
  const input = $('#machine-name .form-control').val();
  if (input.length < maximum_length_of_machine_name) {
    return remove_error();
  }
};

var display_error = function() {
  $('#machine-name .help-block').addClass('has-error');
  $('#machine-name .help-block').addClass('error-text');
  $('#machine-name .help-block').text('Machine name must not exceed 36 characters.');
  return $('#create-machine').prop('disabled', true);
};

var remove_error = function() {
  $('#machine-name .help-block').removeClass('has-error');
  $('#machine-name .help-block').removeClass('error-text');
  $('#machine-name .help-block').text('A name will be generated for you if this field is left blank.');
  return $('#create-machine').prop('disabled', false);
};

const submit_handler = function(e) {
  e.preventDefault();
  return $('form.machine-new-form').submit();
};

// This event listener is triggered from the machines/new.haml form.
$('body').on('machine_new_form', function(e) {
  $('#machine-form-modal').modal('show');
  $('#machine-name .form-control').on('paste', validate_machine_name_on_paste);
  $('#machine-name .form-control').on('keydown', validate_machine_name_on_keydown);
  $('#machine-name .form-control').on('keyup', validate_machine_name_on_keyup);
  $('button#create-machine-submit-button'). on('click', submit_handler);
});
