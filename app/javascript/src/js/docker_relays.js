$(() =>
  $('.delete-relay').on('ajax:success', (e, data, status, xhr) =>
    $(e.target).closest('.docker-relay').remove()
  ).on('ajax:error', (e, xhr, status, error) =>
    Norad.alert('Delete Relay:', 'Could not delete the relay!', 'danger')
  )
);
