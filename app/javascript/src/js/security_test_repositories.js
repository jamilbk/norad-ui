$(function() {
  $(document).on('click', '#submit-repository', function(e) {
    e.preventDefault();
    return $('#repository-form').submit();
  });

  return $(document).on('change', 'input.whitelist-entry', function(e) {
    const $target = $(e.target);
    const $form = $target.closest('form');
    return $form.submit();
  });
});
