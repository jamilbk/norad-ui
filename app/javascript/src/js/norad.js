window.Norad = {
  alert(header, msg, type) {
    // Lazy-create a singleton overlay container
    let $alert;
    let $overlay = $('#alert-overlay');
    if ($overlay.length === 0) {
      $overlay = $('<div id="alert-overlay" class="alert-overlay"></div>')
        .prependTo('.content-wrapper');
    }

    // Create the actual alert box
    const $btn = $('<button type="button" class="close" aria-hidden="true" data-dismiss="alert">x</button>');
    const $header = $('<h4></h4>')
      .text(header);
    return $alert = $('<div class="alert alert-dismissable" style="display: none"></div>')
      .addClass(`alert-${type}`)
      .append($btn, $header, document.createTextNode(msg))
      .on('close.bs.alert', function(e) {
        e.preventDefault();
        return $(this).fadeTo(500, 0).slideUp(300, function() {
          return $(this).trigger('closed.bs.alert');
        });
    }).on('closed.bs.alert', function(e) {
        return $(this).remove();
      }).prependTo($overlay)
      .slideDown();
  },

  remove_tab(e) {
    const $a = $(e.currentTarget).closest('a');
    const pane_id = $a.attr('href');
    const $li = $a.parent();
    $li.prev().find('a').trigger('click');
    $li.remove();
    return $(pane_id).remove();
  },

  destruct_alerts_in(timer) {
    if (!timer) { timer = 3000; }
    return setTimeout(
      () => $('.self-destructable').remove(),
      timer
    );
  }
};
