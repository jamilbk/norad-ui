// Clear any previously set service type options to reset view states
const resetServiceTypeProps = (vm, obj) => {
  obj.ident_required = null

  if (vm.service_type === 'SshService') {
    resetAllWebProps(obj)
    obj.encryption_protocol = 'ssh'
  } else if (vm.service_type === 'WebApplicationService') {
    obj.encryption_protocol = 'cleartext'
  } else if (vm.service_type === 'GenericService') {
    resetAllWebProps(obj)
    obj.encryption_protocol = 'cleartext'
  }
}

const resetAllWebProps = (obj) => {
  resetWebDetailProps(obj)
  resetWebFieldProps(obj)
  resetIdentProps(obj)
}

const resetWebDetailProps = (obj) => {
  obj.web_auth_method = null
  obj.web_start_url = null
  obj.web_blacklist = null
}

const resetWebFieldProps = (obj) => {
  obj.username_field = null
  obj.password_field = null
}

const resetIdentProps = (obj) => {
  obj.username = null
  obj.password = null
}

export function resetServiceProps (vm, e) {
  const changed_prop = e.target.name
  const val = e.target.value

  let props_to_change = {}

  switch (changed_prop) {
    case 'ident_required':
      if (!vm.ident_required)
        resetIdentProps(props_to_change)
      break
    case 'service_type':
      resetServiceTypeProps(vm, props_to_change)
      break
    case 'web_auth_method':
      if (vm.web_auth_method !== 'form') {
        resetWebFieldProps(props_to_change)
        if (vm.web_auth_method === 'unauthenticated')
          resetIdentProps(props_to_change)
      }
  }

  vm.$store.commit('services/service/setProp', props_to_change)
}
