const statusToSortWeight = {
  error: 5,
  fail: 4,
  warn: 3,
  pass: 2,
  info: 1,
  pending: 0
}

// A requirement object is the top-level data structure for holding
// information about its assessments.
const initRequirement = (req) => {
  return {
    id: req.id,
    requirement_name: req.name,
    requirement_description: req.description,
    total_assessments: 0,
    completed_assessments: 0,
    failing_count: 0,
    erroring_count: 0,
    warning_count: 0,
    passing_count: 0,
    informing_count: 0,
    ignoring_count: 0,
    security_tests: {},
  }
}

const initServiceObj = (assessment) => {
  return {
    id: assessment.service_id || -1,
    name: assessment.service_name || '[No Service]',
    results: [],
    overall_status: 'pending'
  }
}

const initMachineObj = (assessment) => {
  return {
    id: assessment.machine_id,
    name: assessment.machine_name,
    services: {},
    overall_status: 'pending'
  }
}

const initSecurityTestObj = (assessment) => {
  return {
    id: assessment.security_container_id,
    title: assessment.title,
    machines: {},
    overall_status: 'pending'
  }
}

const statusSorter = (a, b) => {
  const a_status = a.overall_status || a.status
  const b_status = b.overall_status || b.status
  return statusToSortWeight[b_status] - statusToSortWeight[a_status]
}

const dummy_requirement = {
  name: 'UNGROUPED',
  id: -1,
  description: 'This is a dummy requirement used to group assessments that do not belong to any requirement.',
}

const buildDisplayTitle = (assessment) => {
  const image = assessment.title.split('/')[1]
  const test_name = image.split(':')[0]
  const test_version = image.split(':')[1]

  return `${assessment.repository_name}/${test_name} - v${test_version}`
}

// Update an object's overall status based on status weights.
const updateObjOverallStatus = (obj, result) => {
  const target_status_index = statusToSortWeight[result.status]
  const current_status_index = statusToSortWeight[obj.overall_status]

  // If status is higher severity, update obj overall status with new status
  if (target_status_index > current_status_index && target_status_index != -1) obj.overall_status = result.status
}

const constructResultObj = (requirement, assessment, result) => {
  let test_id = assessment.security_container_id // test and container is 1-1 relationship
  let service_id = assessment.service_id || -1
  let machine_id = assessment.machine_id

  requirement.security_tests[test_id] = requirement.security_tests[test_id] || initSecurityTestObj(assessment)

  let security_test = requirement.security_tests[test_id]
  security_test.display_title = buildDisplayTitle(assessment)
  security_test.machines[machine_id] = security_test.machines[machine_id] || initMachineObj(assessment)

  let machine = security_test.machines[machine_id]
  machine.services[service_id] = machine.services[service_id] || initServiceObj(assessment)

  let service = machine.services[service_id]

  updateObjOverallStatus(machine, result)
  updateObjOverallStatus(security_test, result)
  updateObjOverallStatus(service, result)

  service.results.push(result)
}

const sortResults = (req) => {
  for (let test of Object.values(req.security_tests)) {
    for (let machine of Object.values(test.machines)) {
      for (let service of Object.values(machine.services)) {
        service.results.sort(statusSorter)
      }
      machine.services = Object.values(machine.services).sort(statusSorter)
    }
    test.machines = Object.values(test.machines).sort(statusSorter)
  }
  req.security_tests = Object.values(req.security_tests).sort(statusSorter)
}

export default {
  transformByRequirement (context, raw_assessments) {
    let requirements = {}

    for (let assessment of raw_assessments) {
      if (assessment.associated_requirements.length === 0) assessment.associated_requirements.push(dummy_requirement)

      for (let requirement of assessment.associated_requirements) {
        requirements[requirement.id] = requirements[requirement.id] || initRequirement(requirement)

        requirements[requirement.id].total_assessments += 1
        if (assessment.state === 'complete') requirements[requirement.id].completed_assessments += 1

        for (let result of assessment.results) {
          let req = requirements[requirement.id]

          constructResultObj(req, assessment, result)

          if (result.ignored) {
            req.ignoring_count += 1
            context.commit('ignoreResult', result.signature)
          }
          if (result.status === 'fail') {
            req.failing_count += 1
          }
          if (result.status === 'error') {
            req.erroring_count += 1
          }
          if (result.status === 'warn') {
            req.warning_count += 1
          }
          if (result.status === 'info') {
            req.informing_count += 1
          }
          if (result.status === 'pass') {
            req.passing_count += 1
          }
          result.machine_name = assessment.machine_name
          result.machine_id = assessment.machine_id
        }
      }
    }

    for (let req_id in requirements) {
      sortResults(requirements[req_id])
    }

    return requirements
  }
}
