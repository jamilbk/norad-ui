/*
 * This library is meant to be used in the test environment
 * to aid Capybara in waiting for selectors. It mimics the behavior of jQuery.active.
 */

import axios from 'NodeModules/axios'

const incrementAxiosActive = function() {
  if (window.axiosActive) window.axiosActive++
  else window.axiosActive = 1
}

const decrementAxiosActive = function() {
  if (window.axiosActive) window.axiosActive--
  else window.axiosActive = 0
}

axios.interceptors.request.use(function(config) {
  incrementAxiosActive()
  return config
}, function(error) {
  return Promise.reject(error)
})

axios.interceptors.response.use(function(response) {
  decrementAxiosActive()
  return response
}, function(error) {
  decrementAxiosActive()
  return  Promise.reject(error)
})

export default axios
