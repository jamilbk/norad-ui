import Vue from 'vue'

const capitalize = function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
}

const titleize = function (value) {
  if (!value) return ''
  value = value.toString()
  let parts = value.split(/ |_/)
  parts.forEach((part, i, new_parts) => {
    new_parts[i] = capitalize(part)
  })
  return parts.join(' ')
}

const redact = function (_value) {
  return '••••••••••'
}

Vue.filter('capitalize', capitalize)
Vue.filter('titleize', titleize)
Vue.filter('redact', redact)
