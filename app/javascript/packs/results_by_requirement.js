/* eslint no-console: 0 */

import Vue from 'vue'
import axios from 'axios'
import store from 'store'
import ResultsByRequirement from '../components/results/by_requirement/by_requirement.vue'

Vue.prototype.$http = axios;

document.addEventListener('click', (e) => {
  Vue.prototype.$http.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content');

  let container = document.getElementsByTagName('results-by-requirement')[0]

  if (e.target.id === 'requirement-toggle' && container) {
    let props = {
      docker_command_id: parseInt(container.attributes.docker_command_id.value),
      org_id: parseInt(container.attributes.org_id.value),
      export_queues_present: JSON.parse(container.attributes.export_queues_present.value)
    }

    return new Vue({
      el: 'results-by-requirement',
      store,
      render: function(h) {
        return h(ResultsByRequirement, { props: props })
      }
    })
  }
})
