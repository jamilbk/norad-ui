/* eslint no-console: 0 */

import Vue from 'vue'
import OrganizationsValidator from 'validators/organizations'
import axios from 'axios'
import Orgs from '../components/organizations/organizations.vue'

Vue.prototype.$http = axios
Vue.use(OrganizationsValidator)

document.addEventListener('DOMContentLoaded', () => {
  Vue.prototype.$http.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content')

  new Vue({
    el: 'organizations',
    data: {
      orgs: null,
      default_org: ''
    },
    beforeMount: function(){
      this.orgs = JSON.parse(this.$el.attributes.orgs.value)
      this.default_org = this.$el.attributes.default_org.value
    },
    render: function(h){
      return h(Orgs, { props: { init_orgs: this.orgs, init_default_org: this.default_org } })
    }
  })
})
