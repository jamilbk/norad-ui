/* eslint no-console: 0 */

import Vue from 'vue'
import MachinesValidator from 'validators/machines'
import axios from 'axios'
import MachineList from '../components/machines/machine-list.vue'
import store from 'store'

Vue.prototype.$http = axios
Vue.use(MachinesValidator)

document.addEventListener('DOMContentLoaded', () => {
  Vue.prototype.$http.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content');

  new Vue({
    el: 'machine-list',
    store,
    data: {
      organization_id: null
    },
    render: function(h){
      return h(MachineList, { props: { organizationId: this.$el.attributes.organization_id.value } })
    }
  })
})
