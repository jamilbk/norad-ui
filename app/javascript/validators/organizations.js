import VeeValidate from 'vee-validate'

VeeValidate.Validator.extend('org_name_chars', {
    getMessage: field => 'The organization name is not allowed!',
    validate: (value) => {
      return /^[A-Za-z][A-Za-z0-9 _-]+$/.test(value)
    }
})

VeeValidate.Validator.extend('org_name_default', {
    getMessage: field => 'The organization name cannot end with "default"!',
    validate: (value) => {
      return !(/default$/i.test(value))
    }
})

export default VeeValidate
