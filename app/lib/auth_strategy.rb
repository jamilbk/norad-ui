# frozen_string_literal: true

module AuthStrategy
  class << self
    def reverse_proxy_auth?
      sso_domain.present?
    end

    def local_auth?
      !reverse_proxy_auth?
    end

    def sso_domain
      ENV['SSO_DOMAIN']
    end

    def sso_cookie_name
      'ObSSOCookie'
    end

    def local_user_routes
      reverse_proxy_user_routes + %i[new create]
    end

    def reverse_proxy_user_routes
      %i[edit update show index]
    end
  end
end
