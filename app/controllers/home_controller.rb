# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    redirect_to @user_profile.default_org.blank? ? organizations_path : organization_path(@user_profile.default_org)
  end
end
