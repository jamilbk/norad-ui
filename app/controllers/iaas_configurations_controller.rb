# frozen_string_literal: true

class IaasConfigurationsController < ApplicationController
  def update
    config = IaasConfiguration.new config_params_hash.merge(id: params[:id])
    flash[:danger] = 'Unable to update IaaS configuration.' unless config.save
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to update IaaS configuration.'
  ensure
    redirect_to settings_organization_path(params[:organization_id])
  end

  def create
    org = Organization.new slug: params[:organization_id]
    flash[:danger] = 'Unable to create IaaS configuration.' unless org.iaas_configuration.create(config_params_hash)
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to create Iaas Configuration.'
  ensure
    redirect_to settings_organization_path(org.slug)
  end

  private

  def config_params
    params.require(:iaas_configuration).permit(:provider, :user, :key, :auth_url, :project, :region)
  end

  def config_params_hash
    config_params.to_h
  end
end
