# frozen_string_literal: true

class OrganizationSecurityTestsController < ApplicationController
  layout 'organization', only: :index
  before_action :set_organization, only: %i[index create]

  def index
    @security_containers = OrganizationSecurityContainer.all_by_id(organization_id: @org.db_id)
    @container_configs = @org.security_container_configs
  end

  def create
    requirement = params[:requirement].present? && params[:requirement] == 'true'
    @container_configs = SecurityContainerConfig.create_configs(@org, container_ids, requirement)
  end

  private

  def set_organization
    @org = Organization.find params[:organization_id]
  end

  def container_ids
    params.require(:container_ids)
  end
end
