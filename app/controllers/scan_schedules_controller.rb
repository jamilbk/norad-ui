# frozen_string_literal: true

class ScanSchedulesController < ApplicationController
  private

  def scan_timestamp(p)
    offset = p[:am_pm] == 'PM' ? 12 : 0
    day = p[:frequency].match?(/Daily/i) ? '' : "#{p[:frequency]} "
    day + "#{p[:padded_hour].to_i + offset}:#{p[:padded_minute]}"
  end
end
