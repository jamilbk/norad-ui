# frozen_string_literal: true

class OrganizationScanSchedulesController < ScanSchedulesController
  layout 'organization', only: :index

  def index
    @org = Organization.find params[:organization_id]
  end

  def new
    @schedule = OrganizationScanSchedule.new
  end

  def edit
    @schedule = OrganizationScanSchedule.find(params[:id])
  end

  def update
    schedule = OrganizationScanSchedule.new schedule_params_hash.merge(id: params[:id])
    flash[:danger] = 'Unable to update schedule.' unless schedule.save
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to update schedule.'
  ensure
    redirect_to organization_scan_schedules_path(params[:association_id])
  end

  def create
    org = Organization.new slug: params[:organization_id]
    flash[:danger] = 'Unable to create schedule.' unless org.scan_schedules.create(schedule_params_hash)
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to create schedule.'
  ensure
    redirect_to organization_scan_schedules_path(params[:association_id])
  end

  def destroy
    schedule = OrganizationScanSchedule.find(params[:id])
    flash[:danger] = 'Unable to remove schedule.' unless schedule.destroy
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to remove schedule.'
  ensure
    redirect_to organization_scan_schedules_path(params[:slug]), status: 303
  end

  private

  def schedule_params
    p = params[:organization_scan_schedule]
    p[:period] = p[:frequency].match?(/Daily/i) ? 'daily' : 'weekly'
    p[:at] = scan_timestamp(p)
    params.require(:organization_scan_schedule).permit(:at, :period)
  end

  def schedule_params_hash
    schedule_params.to_h
  end

  def scan_timestamp(p)
    offset = p[:am_pm] == 'PM' ? 12 : 0
    day = p[:frequency].match?(/Daily/i) ? '' : "#{p[:frequency]} "
    day + "#{p[:padded_hour].to_i + offset}:#{p[:padded_minute]}"
  end
end
