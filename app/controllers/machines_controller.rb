# frozen_string_literal: true

class MachinesController < ApplicationController
  layout 'machine', except: [:index]
  before_action :set_machine, only: %i[history results settings update edit]

  def index
    @org = Organization.where(include_assessment_summary: true).find(params[:organization_id])
  rescue NoradApiExceptions::ForbiddenError
    flash.now[:danger] = 'You do not have access to view these machines!'
    render :forbidden
  else
    render layout: 'organization'
  end

  def new
    @org = Organization.find params[:organization_id]
  end

  def edit; end

  def update
    @machine.assign_attributes(machine: machine_params_hash)
    @machine.save!
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    render status: :unprocessable_entity, json: { errors: @machine.pretty_errors.split('.').map(&:strip) }
  end

  def create
    begin
      @org = Organization.find params[:organization_id]
    rescue NoradApiExceptions::NotFoundError
      render status: :not_found, json: { errors: 'Organization ID not found' }
      return
    end

    @machine = @org.machines.create machine: machine_params_hash
    @machine.save!
  rescue Her::Errors::ResourceInvalid, NoradApiExceptions::NoradError
    render status: :unprocessable_entity, json: { errors: @machine.pretty_errors.split('.').map(&:strip) }
  end

  def destroy
    @machine_id = params[:id]
    Machine.destroy_existing(@machine_id)
  rescue Her::Errors::ResourceInvalid, NoradApiExceptions::NoradError
    render status: :unprocessable_entity, json: { errors: 'Error destroying machine' }
  end

  def scan
    scan_response = Machine.scan id: params[:id]
    if scan_response.response_errors.present?
      render(json: { errors: scan_response.pretty_errors }, status: :unprocessable_entity)
    else
      head :ok
    end
  rescue NoradApiExceptions::NoradError
    render(json: { errors: 'Error starting scan' }, status: :unprocessable_entity)
  end

  def history
    @latest_scan_id = @machine.assessments_summary.keys.map(&:to_i).max.to_s
  rescue NoradApiExceptions::ForbiddenError
    flash.now[:danger] = 'You do not have access to view this machine!'
    render :forbidden
  end

  def results
    set_docker_command_and_assessments
    @export_queues_present = @machine.organization.result_export_queues.present?
  rescue NoradApiExceptions::ForbiddenError
    flash.now[:danger] = 'You do not have access to view this machine!'
    render :forbidden
  end

  def settings
    set_new_service_modal
  rescue NoradApiExceptions::ForbiddenError
    flash.now[:danger] = 'You do not have access to view this machine!'
    render :forbidden
  end

  private

  def set_docker_command_and_assessments
    @scan_id = params[:scan_id].to_i

    if @scan_id.zero?
      @assessments = @machine.latest_assessments
      @docker_command = @machine.latest_docker_command
      return
    end

    @assessments = @machine.assessments_by_docker_command_id(@scan_id)
    @docker_command = DockerCommand.find(@scan_id)
  end

  def set_new_service_modal
    # rubocop:disable Style/SafeNavigation
    @render_new_service_modal = params[:render_new_service_modal] && params[:render_new_service_modal].match(/[Tt]rue/)
    # rubocop:enable Style/SafeNavigation
  end

  def redirect_to_machine_creation_success_page(machine)
    flash[:success] = 'Machine successfully created!'
    flash[:warning] = "Please add the services you'd like to test." if machine.services.blank?
    redirect_to machine_services_path(machine)
  end

  def redirect_to_machine_creation_error_page(organization)
    flash[:danger] = "Unable to create machine. #{@machine.pretty_errors}"
    redirect_to organization_machines_path(organization)
  end

  # Strong Parameters Helpers
  def machine_params
    params.require(:machine).permit(:ip, :fqdn, :description, :name, :use_fqdn_as_target)
  end

  def machine_params_hash
    machine_params.to_h
  end
  # End Strong Parameters Helpers

  def set_machine
    @machine = Machine.find params[:id]
    @org = @machine.organization
  end
end
