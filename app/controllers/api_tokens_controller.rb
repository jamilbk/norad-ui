# frozen_string_literal: true

class ApiTokensController < ApplicationController
  def update
    if @current_user.uid == params[:user_id]
      recreate_token
      flash[:success] = 'API Token Recreated!'
    else
      flash[:danger] = "You are not allowed to recreate an API token for #{params[:user_id]}"
    end
    redirect_to user_path(@current_user.uid)
  end

  private

  def recreate_token
    token = ApiToken.new value: @current_user.api_token, user_id: @current_user.uid
    token.save!
    session[:api_token] = token.value
  end
end
