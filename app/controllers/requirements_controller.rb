# frozen_string_literal: true

class RequirementsController < ApplicationController
  before_action :set_requirement, only: :edit

  def create
    group = RequirementGroup.find(params[:requirement_group_id])
    @requirement = group.requirements.create(requirement_params_hash)
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not add requirement!'
  ensure
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @requirement_id = params[:id]
    Requirement.destroy_existing params[:id]
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not remove requirement!'
  ensure
    respond_to do |format|
      format.js
    end
  end

  def edit; end

  def update
    Requirement.save_existing(params[:id], requirement_params_hash)
    flash[:success] = 'Successfully updated requirement.'
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to update requirement.'
  ensure
    redirect_back(fallback_location: { action: 'show', id: params[:id] })
  end

  private

  def requirement_params
    params.require(:requirement).permit(:name, :description)
  end

  def requirement_params_hash
    requirement_params.to_h
  end

  def set_requirement
    @requirement = Requirement.find params[:id]
  end
end
