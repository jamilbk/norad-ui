# frozen_string_literal: true

class IaasDiscoveriesController < ApplicationController
  def create
    if IaasDiscovery.create(organization_id: params[:organization_id])
      flash[:success] = 'Asset discovery scheduled.'
    else
      flash[:danger] = 'Unable to schedule discovery.'
    end
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to schedule discovery'
  ensure
    redirect_to settings_organization_path(params[:organization_id])
  end
end
