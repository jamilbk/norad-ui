# frozen_string_literal: true

class SessionsController < ApplicationController
  layout 'authentication'

  V1_DEPRECATION_MESSAGE = <<~MSG
    Thank you for your interest in Norad.
    To better focus our efforts on version 2,
    we are not allowing new users at this point in time.
  MSG

  private

  def authenticate_user(auth_params)
    user = User.authenticate auth_params
    user_info_to_session(user)
  end

  def user_info_to_session(user)
    reset_session
    session[:api_token] = user.api_token
    session[:user_uid] = user.uid
    session[:user_id] = user.id
    session[:expires_at] = Time.zone.now + Rails.configuration.x.session_timeout
  end

  def redirect_for_logout_success
    flash[:success] = 'Thank you for visiting. You are now logged out.'
    redirect_to terminated_session_path
  end
end
