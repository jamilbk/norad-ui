# frozen_string_literal: true

class SecurityContainerConfigsController < ApplicationController
  def show
    @config = SecurityContainerConfig.find params[:id]
    @container = SecurityContainer.find @config.security_container_id
  end

  def update
    @config = SecurityContainerConfig.find params[:id]
    @config.values = value_params
    @config.save!
    @msg = { head: 'Config Update', body: 'Saved Successfully', type: 'success' }
  rescue Her::Errors::ResourceInvalid, NoradApiExceptions::NoradError, ActionController::ParameterMissing
    @msg = { head: 'Config Update', body: 'Error while saving', type: 'danger' }
  end

  def destroy
    @scc = SecurityContainerConfig.find params[:id]
    @scc.destroy if @scc.present?
  end

  private

  def value_params
    params[:values].tap do |vals|
      vals.select! { |key| @config.args.include?(key) }.permit!
    end
    params.require(:values).permit!.to_h
  end
end
