# frozen_string_literal: true

class SshKeyPairAssignmentsController < ApplicationController
  def create
    assignment = SshKeyPairAssignment.create(machine_id: params[:machine_id], ssh_key_pair_id: params[:ssh_key_pair_id])
    flash[:danger] = 'Unable to assign SSH Key Pair to Machine.' unless assignment.id
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to assign SSH Key Pair to Machine.'
  ensure
    redirect_to create_redirect_path
  end

  def destroy
    # XXX: Note: the id of 1 does not matter here because the resource path
    # for the SshKeyPairAssignment Her model doesn't use and ID
    SshKeyPairAssignment.destroy_existing(1, machine_id: params[:machine_id])
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to remove SSH Key Pair Assignment'
  ensure
    redirect_to settings_machine_path(params[:machine_id])
  end

  private

  def create_redirect_path
    machine = Machine.find params[:machine_id]
    settings_machine_path(params[:machine_id], render_new_service_modal: machine.key_without_ssh_service?)
  end
end
