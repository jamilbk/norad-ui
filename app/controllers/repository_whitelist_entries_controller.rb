# frozen_string_literal: true

class RepositoryWhitelistEntriesController < ApplicationController
  before_action :load_repository
  before_action :load_org_id

  # POST /security_test_repositories/:security_test_repository_id/repository_whitelist_entries
  def create
    @entry = @repository.repository_whitelist_entries.create(entry_params_hash)
  rescue NoradApiExceptions::NoradError => e
    @error_msg = "Unable to whitelist repository. Error details: #{e.message}"
  else
    @error_msg = @entry.pretty_errors
  end

  # DELETE /security_test_repositories/:security_test_repository_id/repository_whitelist_entries/:id
  def destroy
    RepositoryWhitelistEntry.destroy_existing params[:id],
                                              security_test_repository_id: params[:security_test_repository_id]
  rescue NoradApiExceptions::NoradError => e
    @error_msg = "Unable to remove repository from whitelist. Error details: #{e.message}"
  end

  private

  def load_repository
    @repository ||= SecurityTestRepository.find(params[:security_test_repository_id])
  end

  # Always pass along org_id so it can be used by whitelist_entry_form helper
  def load_org_id
    @org_id = params[:organization_id]
  end

  def entry_params
    params.require(:repository_whitelist_entry).permit(:organization_id)
  end

  def entry_params_hash
    entry_params.to_h
  end
end
