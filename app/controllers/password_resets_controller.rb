# frozen_string_literal: true

class PasswordResetsController < ApplicationController
  layout 'authentication'

  skip_around_action :set_user_api_token
  skip_before_action :require_login
  skip_before_action :set_user_profile

  def new
    @password_reset = PasswordReset.new
  end

  def create
    @password_reset = PasswordReset.create(create_params_hash)
    flash[:success] = 'If that email exists in the system it will receive a password reset link.'
    redirect_to login_path
  end

  def edit
    @password_reset = PasswordReset.find(params[:provided_token])
  rescue NoradApiExceptions::NotFoundError
    flash[:danger] = 'That was an invalid or expired password reset code.'
    redirect_to login_path
  end

  def update
    @password_reset = PasswordReset.save_existing(params[:provided_token], update_params_hash)
    if @password_reset.response_errors.blank?
      reset_session
      flash[:success] = 'Password updated successfully. You may now sign in.'
      redirect_to login_path
    else
      flash[:danger] = 'There was an error updating your password.'
      render :edit
    end
  end

  private

  def update_params
    params.require(:password_reset).permit(:password, :password_confirmation)
  end

  def update_params_hash
    update_params.to_h
  end

  def create_params
    params.require(:password_reset).permit(:email)
  end

  def create_params_hash
    create_params.to_h
  end
end
