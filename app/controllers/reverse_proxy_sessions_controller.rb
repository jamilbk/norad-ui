# frozen_string_literal: true

class ReverseProxySessionsController < SessionsController
  before_action -> { redirect_to root_url if user_signed_in? }, only: :new

  skip_before_action :require_login, only: %i[new create]
  skip_before_action :set_user_profile, only: %i[new create]
  skip_around_action :set_user_api_token, only: %i[new create]

  def new
    prod_new if production?
  end

  def create
    # With SSO, this method is not necessary so just send back a 204
    head :no_content && return if production?

    authenticate_user(login_params_hash)
    redirect_to root_url
  rescue NoradApiExceptions::ForbiddenError
    flash.now[:danger] = V1_DEPRECATION_MESSAGE
    render :new
  rescue StandardError
    flash.now[:danger] = 'Invalid login attempt'
    render :new
  end

  def destroy
    reset_session
    clear_sso_cookie if production?
    redirect_for_logout_success
  end

  private

  def login_params
    params.require(:session).permit(:uid, :password)
  end

  def login_params_hash
    login_params.to_h
  end

  def clear_sso_cookie
    cookies.delete AuthStrategy.sso_cookie_name, domain: AuthStrategy.sso_domain
  end

  def sso_authenticate_user
    authenticate_user(prod_params_hash)
    redirect_to root_url
  rescue NoradApiExceptions::ForbiddenError
    clear_sso_cookie
    flash.now[:danger] = V1_DEPRECATION_MESSAGE
    render html: nil, layout: true
  rescue StandardError
    clear_sso_cookie
    redirect_to root_url
  end

  def prod_params
    ActionController::Parameters.new(uid: request.headers['HTTP_PI_USER']).permit(:uid)
  end

  def prod_params_hash
    prod_params.to_h
  end

  def production?
    Rails.env.production?
  end

  def prod_new
    sso_authenticate_user
  end
end
