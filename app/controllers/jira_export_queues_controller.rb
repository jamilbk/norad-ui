# frozen_string_literal: true

class JiraExportQueuesController < ResultExportQueuesController
  def new
    @org_id = params[:organization_id]
  end

  def create
    build_params = jira_export_queue_params_hash.merge(organization_id: params[:organization_id])
    JiraExportQueue.build(build_params).save!
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    flash[:danger] = 'Unable to create Jira export location'
  ensure
    fallback_path = params[:organization_id] ? settings_organization_path(params[:organization_id]) : root_path
    redirect_back(fallback_location: fallback_path)
  end

  def edit
    @export_queue = JiraExportQueue.find(params[:id])
    @org_id = @export_queue.organization_id
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    @export_queue = nil
    @org_id = nil
  end

  def update
    jira_export_queue = JiraExportQueue.find(params[:id])
    org_id = jira_export_queue.organization_id
    jira_export_queue.assign_attributes(jira_export_queue_update_params_hash)
    jira_export_queue.save!
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    flash[:danger] = 'Unable to update Jira export location'
  ensure
    fallback_path = org_id ? settings_organization_path(org_id) : root_path
    redirect_back(fallback_location: fallback_path)
  end

  def destroy
    @pid = params[:id]
    JiraExportQueue.destroy_existing(params[:id])
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    @pid = nil
  end

  private

  def jira_export_queue_params_hash
    jira_export_queue_params.to_h
  end

  def jira_export_queue_params
    params.require(:jira_export_queue)
          .permit([
                    :auto_sync,
                    custom_jira_configuration_attributes: %i[id title site_url project_key username password]
                  ])
  end

  def jira_export_queue_update_params_hash
    jira_export_queue_params.to_h.tap do |hsh|
      pw = hsh['custom_jira_configuration_attributes']['password']
      hsh['custom_jira_configuration_attributes'].delete('password') if pw.blank?
    end
  end
end
