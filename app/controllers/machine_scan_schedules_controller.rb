# frozen_string_literal: true

class MachineScanSchedulesController < ScanSchedulesController
  def new
    @schedule = MachineScanSchedule.new
  end

  def edit
    @schedule = MachineScanSchedule.find(params[:id])
  end

  def update
    schedule = MachineScanSchedule.new schedule_params_hash.merge(id: params[:id])
    flash[:danger] = 'Unable to update schedule.' unless schedule.save
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to update schedule.'
  ensure
    redirect_to settings_machine_path(params[:association_id])
  end

  def create
    machine = Machine.new id: params[:machine_id]
    flash[:danger] = 'Unable to create schedule.' unless machine.scan_schedules.create(schedule_params_hash)
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to create schedule.'
  ensure
    redirect_to settings_machine_path(params[:machine_id])
  end

  def destroy
    schedule = MachineScanSchedule.find(params[:id])
    flash[:danger] = 'Unable to remove schedule.' unless schedule.destroy
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to remove schedule.'
  ensure
    redirect_to settings_machine_path(params[:machine_id])
  end

  private

  def schedule_params
    p = params[:machine_scan_schedule]
    p[:period] = p[:frequency].match?(/Daily/i) ? 'daily' : 'weekly'
    p[:at] = scan_timestamp(p)
    params.require(:machine_scan_schedule).permit(:at, :period)
  end

  def schedule_params_hash
    schedule_params.to_h
  end
end
