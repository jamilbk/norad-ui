# frozen_string_literal: true

class DockerRelaysController < ApplicationController
  layout 'docker_relay', only: :index

  def index
    @org = Organization.find params[:organization_id]

    # Eager-load the relays by calling #reload here. Otherwise, Her will delay raising this exception
    # and rescue will have no effect.
    @relays = @org.docker_relays.reload
  rescue NoradApiExceptions::ForbiddenError
    @relays = nil
  end

  def update
    DockerRelay.save_existing(params[:id], verified: update_params[:verified])

    head :no_content
  end

  def destroy
    DockerRelay.destroy_existing(params[:id])
  rescue NoradApiExceptions::NoradError
    head 400
  else
    head :no_content
  end

  private

  def update_params
    params.require(:docker_relay).permit(:verified)
  end
end
