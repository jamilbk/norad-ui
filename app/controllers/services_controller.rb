# frozen_string_literal: true

class ServicesController < ApplicationController
  include ServiceIdentityControllerHelpers
  include WebAppConfigControllerHelpers
  include ServiceParamsHelpers

  before_action :set_machine, only: %i[index create]
  layout 'machine', only: :index

  def index
    @org = @machine.organization
    @services = @machine.services
    @service_discovery = @machine.latest_service_discovery
    flash.now[:success] = 'Machine successfully created!' if params[:machine_created]
    flash.now[:warning] = 'Service discovery in progress. Check back later.' if @machine.scheduled_service_discovery
  end

  def new
    set_modal_warning_message
    @machine_id = params.require(:machine_id)
  end

  def create
    respond_to do |format|
      format.html { service_create_html }
      format.json { service_create_json }
    end
  end

  def edit
    @machine_id = params[:machine_id]
    @service = Service.find(params[:id])
    # XXX The way which @service_identity & @web_application_config is set is due to strange behavior
    # in the Her gem (as of v 0.8.2). Until that behavior is fixed this will need to stay as is
    @service_identity = @service.service_identity.nil? ? ServiceIdentity.new : @service.service_identity
    @web_application_config = resolve_web_app_config
  end

  def update
    service = save_existing_service
    return flash[:danger] = service.pretty_errors if service.response_errors.present?
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    flash[:danger] = 'Unable to update service'
  ensure
    redirect_to machine_services_path(params[:machine_id])
  end

  def destroy
    @sid = params[:id]
    Service.destroy_existing(@sid)
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    @sid = nil
  end

  private

  def save_existing_service
    Service.save_existing(params[:id], service_update_params)
  end

  def attempt_service_create
    @service = Service.build(service_create_params.merge(machine_id: @machine.id))
    @service.save!
  end

  def service_create_html
    attempt_service_create
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    flash[:danger] = 'Unable to save service'
  ensure
    redirect_to_post_create_path
  end

  def service_create_json
    attempt_service_create
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    render json: { errors: @service.pretty_errors }, status: :unprocessable_entity
  else
    head :created
  end

  def redirect_to_post_create_path
    unless @machine.ssh_service_without_key? && created_service_was_ssh?
      redirect_to machine_services_path(@machine)
      return
    end

    flash[:warning] = "You have added an SSH Service to machine \"#{@machine.name}\", "\
                      'but no SSH Key Pair is available. It is recommended to assign one.'
    redirect_to settings_machine_path(@machine)
  end

  def created_service_was_ssh?
    params[:service][:type] == 'SshService'
  end

  def set_modal_warning_message
    @show_ssh_service_warning = params[:show_ssh_service_warning]&.match(/[Tt]rue/)
  end

  def set_machine
    @machine = Machine.find params[:machine_id]
  end
end
