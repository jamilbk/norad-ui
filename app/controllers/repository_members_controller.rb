# frozen_string_literal: true

class RepositoryMembersController < ApplicationController
  def create
    @member = RepositoryMember.create(creation_params_hash)
    @error_msg = @member.pretty_errors if @member.response_errors.present?
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not add repository member!'
  end

  def destroy
    @member_id = params[:id]
    RepositoryMember.destroy_existing(@member_id)
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not remove repository member!'
  end

  private

  def member_params
    params.require(:repository_member).permit(:uid, :role_type)
  end

  def creation_params
    member_params.merge(security_test_repository_id: params[:security_test_repository_id])
  end

  def creation_params_hash
    creation_params.to_h
  end
end
