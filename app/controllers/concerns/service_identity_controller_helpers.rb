# frozen_string_literal: true

module ServiceIdentityControllerHelpers
  extend ActiveSupport::Concern

  included do
    private

    def identity_params
      params.require(:service_identity).permit(:username, :password)
    end

    def identity_params_hash
      identity_params.to_h
    end

    def identity_params_blank?
      return true if params[:service_identity].blank?
      identity_params[:username].blank? && identity_params[:password].blank?
    end
  end
end
