# frozen_string_literal: true

class InfosecExportQueuesController < ResultExportQueuesController
  def new
    @org_id = params[:organization_id]
  end

  def create
    create_new_infosec_export_queue
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to create Infosec export location'
  ensure
    fallback_path = params[:organization_id] ? settings_organization_path(params[:organization_id]) : root_path
    redirect_back(fallback_location: fallback_path)
  end

  def edit
    @export_queue = InfosecExportQueue.find(params[:id])
    @org_id = @export_queue.organization_id
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    @export_queue = nil
    @org_id = nil
  end

  def update
    infosec_export_queue = InfosecExportQueue.find(params[:id])
    org_id = infosec_export_queue.organization_id
    infosec_export_queue.assign_attributes(infosec_export_queue_params_hash)
    flash[:danger] = infosec_export_queue.pretty_errors unless infosec_export_queue.save
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to update Infosec export location'
  ensure
    fallback_path = org_id ? settings_organization_path(org_id) : root_path
    redirect_back(fallback_location: fallback_path)
  end

  def destroy
    @pid = params[:id]
    InfosecExportQueue.destroy_existing(params[:id])
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    @pid = nil
  end

  private

  def create_new_infosec_export_queue
    build_params = infosec_export_queue_params.merge(organization_id: params[:organization_id])
    infosec_export_queue = InfosecExportQueue.build(build_params)
    flash[:danger] = infosec_export_queue.pretty_errors unless infosec_export_queue.save
  end

  def infosec_export_queue_params_hash
    infosec_export_queue_params.to_h
  end

  def infosec_export_queue_params
    params.require(:infosec_export_queue)
          .permit([
                    :auto_sync,
                    infosec_export_queue_configuration_attributes: %i[id ctsm_id]
                  ])
  end
end
