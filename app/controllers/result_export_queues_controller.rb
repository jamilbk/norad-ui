# frozen_string_literal: true

class ResultExportQueuesController < ApplicationController
  def new
    @org_id = params[:organization_id]
  end
end
