# frozen_string_literal: true

class TerminatedSessionsController < ApplicationController
  skip_before_action :require_login
  skip_before_action :set_user_profile
  skip_around_action :set_user_api_token

  layout 'authentication'

  def show
    flash.now[:success] = 'Welcome to Norad!' if flash.empty?
  end
end
