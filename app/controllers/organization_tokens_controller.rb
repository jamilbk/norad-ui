# frozen_string_literal: true

class OrganizationTokensController < ApplicationController
  def update
    recreate_token
  rescue NoradApiExceptions::ForbiddenError
    flash[:danger] = 'You are not allowed to recreate tokens without admin permissions!'
  ensure
    redirect_to organization_path(params[:organization_id])
  end

  private

  def recreate_token
    token = OrganizationToken.new value: 'default-value', organization_id: params[:organization_id]
    token.save!
  end
end
