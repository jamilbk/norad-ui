# frozen_string_literal: true

class IaasDiscovery
  include Her::Model
  include_root_in_json true

  belongs_to :iaas_configuration

  collection_path 'organizations/:organization_id/iaas_discoveries'
  resource_path 'iaas_iaas_discoveries/:id'
end
