# frozen_string_literal: true

class DockerCommandMachineSummary
  include Her::Model

  resource_path 'docker_commands/:id/machine_summary'
end
