# frozen_string_literal: true

class ServiceIdentity
  include Her::Model
  include_root_in_json true

  attributes :username, :password
  collection_path 'services/:service_id/service_identities'
  resource_path 'service_identities/:id'

  belongs_to :service
end
