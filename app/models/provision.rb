# frozen_string_literal: true

class Provision
  include Her::Model
  include_root_in_json true

  belongs_to :security_container
  belongs_to :requirement

  collection_path '/v1/requirements/:requirement_id/provisions'
  resource_path '/v1/provisions/:id'

  def self.create_provisions(requirement, container_ids)
    valid_container_ids(container_ids, requirement).map do |cid|
      Provision.create(requirement_id: requirement.id, security_container_id: cid)
    end
  end

  def self.valid_container_ids(new_ids, requirement)
    existing_ids = requirement.provisions.map(&:security_container_id)
    new_ids.map(&:to_i) - existing_ids
  end

  private_class_method :valid_container_ids
end
