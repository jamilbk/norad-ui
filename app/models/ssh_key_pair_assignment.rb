# frozen_string_literal: true

class SshKeyPairAssignment
  include Her::Model
  collection_path 'machines/:machine_id/ssh_key_pair_assignment'
  resource_path 'machines/:machine_id/ssh_key_pair_assignment'

  belongs_to :machine
  belongs_to :ssh_key_pair
end
