# frozen_string_literal: true

class RepositoryMember
  include Her::Model
  include PrettyErrors

  include_root_in_json true
  collection_path 'security_test_repositories/:security_test_repository_id/repository_members'
  resource_path 'repository_members/:id'

  has_one :user
end
