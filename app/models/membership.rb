# frozen_string_literal: true

class Membership
  include Her::Model
  include PrettyErrors

  include_root_in_json true
  collection_path 'organizations/:organization_id/memberships'
  resource_path 'memberships/:id'

  belongs_to :organization
  belongs_to :user
end
