# frozen_string_literal: true

class JiraExportQueue < ResultExportQueue
  # we needs this here becaues of
  # https://github.com/remiprev/her/issues/461
  store_response_errors :response_errors

  collection_path 'organizations/:organization_id/jira_export_queues'
  resource_path 'jira_export_queues/:id'
  include_root_in_json true

  belongs_to :organization

  def config
    custom_jira_configuration
  end

  def title
    config['title']
  end
end
