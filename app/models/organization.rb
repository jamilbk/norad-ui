# frozen_string_literal: true

class Organization
  include Her::Model
  include PrettyErrors
  include Commandable
  primary_key :slug
  include_root_in_json true

  has_many :machines
  has_many :security_container_configs, inverse_of: :organization, class_name: 'OrganizationContainerConfig'
  has_many :security_containers, inverse_of: :organization, class_name: 'OrganizationSecurityContainer'
  has_many :docker_relays
  has_one :configuration, inverse_of: :organization, class_name: 'OrganizationConfiguration'
  has_many :docker_commands, inverse_of: :organization, class_name: 'OrganizationDockerCommand'
  has_many :memberships
  has_many :result_ignore_rules, inverse_of: :organization, class_name: 'OrganizationResultIgnoreRule'
  has_many :scan_schedules, inverse_of: :organization, class_name: 'OrganizationScanSchedule'
  has_many :requirements, inverse_of: :organization, class_name: 'OrganizationRequirement'
  has_many :enforcements, inverse_of: :organization, class_name: 'OrganizationEnforcement'
  has_one :iaas_configuration
  has_many :ssh_key_pairs
  has_many :result_export_queues
  has_many :notification_channels
  has_many :organization_errors

  custom_post :scan

  def configuration_errors?
    organization_errors.present?
  end

  def can_scan_be_disabled?
    false
  end

  def assessments_summary
    OrganizationScanSummary.find(id).assessments_summary
  end

  def safe_update_uid(parameters)
    original_slug = slug
    assign_attributes(parameters)
    save!
    # Returns number of users setting this org as default
    UserProfile.update_defaults(original_slug, self)
    self
  end

  def security_tests_configured?
    requirements.present? || security_container_configs.present?
  end

  def db_id
    attributes[:id]
  end

  def to_h
    { uid: uid, slug: slug, token: token, machine_count: machine_count }
  end
end
