# frozen_string_literal: true

class ResultExporter
  include Her::Model

  collection_path 'result_exporters'
end
