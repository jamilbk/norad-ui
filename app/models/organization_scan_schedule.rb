# frozen_string_literal: true

class OrganizationScanSchedule < ScanSchedule
  belongs_to :organization
  collection_path 'organizations/:organization_id/scan_schedules'
  resource_path 'organization_scan_schedules/:id'
end
