# frozen_string_literal: true

class WebApplicationConfig
  include Her::Model
  include_root_in_json true

  attributes :auth_type, :url_blacklist, :starting_page_path,
             :login_form_username_field_name, :login_form_password_field_name
  collection_path 'services/:service_id/web_application_configs'
  resource_path 'web_application_configs/:id'

  belongs_to :service
end
