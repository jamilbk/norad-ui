# frozen_string_literal: true

class OrganizationResultIgnoreRule
  include Her::Model
  collection_path 'organizations/:organization_id/result_ignore_rules'
  resource_path 'organizations/:organization_id/result_ignore_rules/:id'
end
