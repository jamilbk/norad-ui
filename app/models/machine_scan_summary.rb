# frozen_string_literal: true

class MachineScanSummary
  include Her::Model

  resource_path 'machines/:id/scan_summary'
end
