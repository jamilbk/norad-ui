# frozen_string_literal: true

class InfosecExportQueue < ResultExportQueue
  include PrettyErrors

  # we needs this here becaues of
  # https://github.com/remiprev/her/issues/461
  store_response_errors :response_errors

  collection_path 'organizations/:organization_id/infosec_export_queues'
  resource_path 'infosec_export_queues/:id'
  include_root_in_json true

  belongs_to :organization

  def config
    infosec_export_queue_configuration
  end

  def title
    'Cisco Infosec'
  end
end
