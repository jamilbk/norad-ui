# frozen_string_literal: true

class NotificationChannel
  include Her::Model
  collection_path 'organizations/:organization_id/notification_channels'
  resource_path 'notification_channels/:id'
  include_root_in_json true

  belongs_to :organization
end
