# frozen_string_literal: true

class Result
  include Her::Model
  belongs_to :assessment
end
