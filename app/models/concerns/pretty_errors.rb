# frozen_string_literal: true

# A simple module for formatting the errors returned by the API. Her doesn't properly instantiate the
# ActiveModel::Errors object so we need to process resource.response_errors ourselves
module PrettyErrors
  extend ActiveSupport::Concern

  included do
    def pretty_errors
      formatted_errors = respond_to?(:response_errors) ? response_errors_to_human : errors.full_messages.join('. ')
      return if formatted_errors.blank?
      "#{formatted_errors}."
    end

    private

    # Her::Model#response_errors returns a Hash or Array like:
    # {
    #   :"local_authentication_method.local_authentication_record"=>["is invalid"],
    #   :"local_authentication_method.local_authentication_record.password_confirmation"=>["doesn't match Password"]
    # }
    # So we need to build the full messages ourselves :-(
    def response_errors_to_human
      ResponseErrorsParser.new(response_errors).to_human
    end
  end
end
