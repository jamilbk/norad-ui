# frozen_string_literal: true

class User
  include Her::Model
  include PrettyErrors

  primary_key :uid
  include_root_in_json true

  custom_post :authenticate

  def fullname
    "#{firstname} #{lastname}"
  end

  def db_id
    attributes[:id]
  end
end
