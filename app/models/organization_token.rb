# frozen_string_literal: true

class OrganizationToken
  include Her::Model
  primary_key :value
  resource_path 'organizations/:organization_id/organization_token'
end
