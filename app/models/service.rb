# frozen_string_literal: true

class Service
  include Her::Model
  include PrettyErrors
  include_root_in_json true

  collection_path 'machines/:machine_id/services'
  resource_path 'services/:id'

  belongs_to :machine
  has_one :service_identity
  belongs_to :application_type
  has_one :web_application_config

  def web_app?
    type == 'WebApplicationService'
  end

  def identity?
    service_identity.nil? ? false : true
  end

  def application_display_name
    app_type = self.class.transform_https_to_http(application_type)
    return 'Other' if app_type.nil?
    self.class.translation_hash.dig(app_type.name, app_type.port) ? app_type.name : 'Other'
  end

  def self.service_display_info(service)
    app_type = transform_https_to_http(service.application_type)
    return if app_type.nil?
    name =
      translation_hash.dig(app_type.name, app_type.port, app_type.transport_protocol)
    name
  end

  def self.application_type_options
    ::PrecachedValues.retrieve_application_type_options
  end

  def self.translation_hash
    ::PrecachedValues.retrieve_translation_hash
  end

  def self.transform_https_to_http(app_type)
    return if app_type.nil?
    return app_type unless app_type.name == 'https'
    ApplicationType.new(name: 'http', port: 80, transport_protocol: app_type.transport_protocol)
  end
end
