# frozen_string_literal: true

class OrganizationConfiguration
  include Her::Model
  include_root_in_json true

  belongs_to :organization
end
