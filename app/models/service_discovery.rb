# frozen_string_literal: true

class ServiceDiscovery
  include Her::Model
  include_root_in_json true

  belongs_to :machine

  collection_path 'machines/:machine_id/service_discoveries'
  resource_path 'service_discoveries/:id'
end
