# frozen_string_literal: true

class SshConfig
  include Her::Model
  collection_path 'machines/:machine_id/ssh_configs'
  resource_path 'ssh_configs/:id'
  include_root_in_json true

  belongs_to :machine
end
