# frozen_string_literal: true

class Assessment
  include Her::Model
  collection_path 'machines/:machine_id/assessments'
  resource_path 'assessments/:id'

  has_many :results

  def self.statistics(assessments_collection)
    return Hash.new(0) if assessments_collection.empty?
    as = assessment_statistics(assessments_collection)
    rs = results_statistics(assessments_collection)
    as.merge(rs)
  end

  def statistics
    state_array = results.map(&:status).each_with_object(Hash.new(0)) { |status, counts| counts[status] += 1 }
    ignored = results.map(&:ignored).count(true)
    {
      failed: state_array['fail'],
      errored: state_array['error'],
      warned: state_array['warn'],
      passed: state_array['pass'],
      info: state_array['info'],
      ignored: ignored
    }
  end

  def self.results_statistics(assessments_collection)
    array_of_individual_statistics = assessments_collection.map(&:statistics)
    sum_list_of_hashes_by_key(array_of_individual_statistics)
  end

  def self.assessment_statistics(assessments_collection)
    incomplete = assessments_collection.count { |assessment| assessment.results == [] }
    { incomplete: incomplete, total: assessments_collection.count }
  end

  def self.sum_list_of_hashes_by_key(list_of_hashes)
    list_of_hashes.inject do |aggregate, new_hash|
      aggregate.merge(new_hash) { |_key, old_value, new_value| old_value + new_value }
    end
  end

  private_class_method :results_statistics
  private_class_method :assessment_statistics
  private_class_method :sum_list_of_hashes_by_key
end
