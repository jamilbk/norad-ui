# frozen_string_literal: true

class ApiToken
  include Her::Model
  primary_key :value
  resource_path 'users/:user_id/api_token'
end
