# frozen_string_literal: true

class OrganizationContainerConfig
  include Her::Model
  collection_path 'organizations/:organization_id/security_container_configs'
  resource_path 'security_container_configs/:id'

  belongs_to :security_container
end
