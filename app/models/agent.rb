# frozen_string_literal: true

class Agent
  include Her::Model

  belongs_to :machine
end
