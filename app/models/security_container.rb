# frozen_string_literal: true

class SecurityContainer
  include Her::Model

  DOCKER_TAG_DELIMITER = /[@:]/

  include_root_in_json true

  collection_path 'security_containers'
  resource_path 'security_containers/:id'

  belongs_to :security_test_repository

  def self.all_by_id(opts = {})
    all(opts).each_with_object({}) do |config, h|
      h[config.id] = config
    end
  end

  def service_requirements
    "#{display_service_name}#{encryption_type}#{brute_force_status}"
  end

  private

  def display_service_name
    return application_type['name'].capitalize if application_type.present?
    return 'Authenticated SSH service' if test_types.include? 'authenticated'
    return 'Web application service' if test_types.include? 'web_application'
    return 'Service' unless test_types.include? 'whole_host'
    ''
  end

  def encryption_type
    return ' using SSL/TLS encryption type' if display_ssl_crypto?
    return ' using SSH encryption type' if display_ssh_crypto?
    ''
  end

  def brute_force_status
    return ' with brute force enabled' if test_types.include? 'brute_force'
    ''
  end

  def display_ssl_crypto?
    test_types.include? 'ssl_crypto'
  end

  def display_ssh_crypto?
    (test_types.include? 'ssh_crypto') && !(test_types.include? 'authenticated')
  end
end
