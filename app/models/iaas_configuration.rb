# frozen_string_literal: true

class IaasConfiguration
  include Her::Model
  include_root_in_json true

  VALID_PROVIDERS = %w[OpenStack AWS].freeze

  belongs_to :organization
  has_many :iaas_discoveries

  collection_path 'organizations/:organization_id/iaas_configuration'
  resource_path 'iaas_configurations/:id'
end
