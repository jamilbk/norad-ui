# frozen_string_literal: true

class RepositorySecurityContainer < SecurityContainer
  include PrettyErrors
  include CollectionCreatable

  collection_path 'security_test_repositories/:security_test_repository_id/security_containers'
  resource_path 'security_containers/:id'

  include_root_in_json :security_container

  belongs_to :security_test_repository

  class << self
    def create_multiple(hashes, security_test_repository_id)
      omit_root_from_json
      url = "security_test_repositories/#{security_test_repository_id}/security_containers"
      create_collection(url, security_containers: hashes)
    end

    private

    def omit_root_from_json
      include_root_in_json false
      SecurityContainer.include_root_in_json false
    end
  end
end
