# frozen_string_literal: true

class MachineContainerConfig
  include Her::Model
  collection_path 'machines/:machine_id/security_container_configs'
  resource_path 'security_container_configs/:id'
end
