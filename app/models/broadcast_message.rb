# frozen_string_literal: true

class BroadcastMessage < ApplicationRecord
  validates :severity, inclusion: { in: %w[danger info warning success] }
  validates :live, uniqueness: true, if: :live?

  def self.turn_off
    where(live: true).update(live: false)
  end

  def self.create_and_activate(attrs = {})
    record = nil
    transaction do
      turn_off
      record = create!(attrs.merge(live: true))
    end
    record
  end
end
