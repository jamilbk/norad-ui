# frozen_string_literal: true

class RepositoryWhitelistEntry
  include Her::Model
  include PrettyErrors

  include_root_in_json true
  collection_path 'security_test_repositories/:security_test_repository_id/repository_whitelist_entries'
  resource_path 'repository_whitelist_entries/:id'

  belongs_to :organization
end
