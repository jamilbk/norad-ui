# frozen_string_literal: true

class MachineDockerCommand
  include Her::Model
  collection_path 'machines/:machine_id/docker_commands'
  resource_path 'docker_commands/:id'

  has_many :assessments
end
