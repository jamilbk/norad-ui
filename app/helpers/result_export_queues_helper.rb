# frozen_string_literal: true

module ResultExportQueuesHelper
  def format_queue_type(queue_string)
    return_value = queue_string.gsub('ExportQueue', '')
    return_value = 'Cisco Infosec' if return_value == 'Infosec'
    return_value
  end
end
