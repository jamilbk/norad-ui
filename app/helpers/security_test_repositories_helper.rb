# frozen_string_literal: true

module SecurityTestRepositoriesHelper
  def scope_for(repository)
    repository.public ? 'Public' : 'Private'
  end

  def repository_form_for(repository)
    remote = action_name == 'new'
    form_for(
      repository || SecurityTestRepository.new,
      html: { class: 'form-horizontal', id: 'repository-form', remote: remote }
    ) do
      yield
    end
  end

  def show_repository_details?(repository)
    repository.public || repository.authorizer&.fetch(:can_edit?)
  end

  def whitelist_entry_form_for(repository, org_id, checked)
    html_opts = { remote: true }
    html_opts[:method] = :delete if checked
    entry = checked ? repository.entry_for_org_id(org_id) : RepositoryWhitelistEntry.new
    form_for [repository, entry], html: html_opts do
      yield
    end
  end
end
