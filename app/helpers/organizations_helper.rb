# frozen_string_literal: true

module OrganizationsHelper
  def short_key_signature(key_sig)
    key_sig.split(':')[-8..-1].join(':').upcase
  end

  def relay_state_color(state)
    state == 'online' ? 'label-success' : 'label-default'
  end

  def auto_approve?(org)
    org.configuration.auto_approve_docker_relays
  end

  def relay_ssh_key?(org)
    org.configuration.use_relay_ssh_key
  end

  def disable_if_reader(admin_status)
    admin_status ? '' : 'disabled'
  end

  def passing_percentage(org, summary)
    return 0 if org.machine_count.zero?
    # "pending" isn't technically passing, but we are treating it as such in this simple display
    100 * (summary[:passing] + summary[:informing] + summary[:pending]) / org.machine_count
  end

  def organization_name_help_string
    'Organization names must begin with a letter and may be followed by ' \
    'letters, numbers, spaces, and dashes.'
  end

  def orgs_to_json(orgs)
    orgs.each_with_object({}) { |org, memo| memo[org.slug] = org.to_h }.to_json
  end
end
