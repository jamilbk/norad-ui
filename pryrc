require 'rubygems'

# This file helps when using the rails console
# so you don't have to log in every time

# To use, put your user name in place of <your-username-here>
# Then run the following on the command line
# `cp ./pryrc ./.pryrc`

begin
  user = User.authenticate uid: <your-username-here>, password: 'somepassword'
  RequestStore[:api_token] = user.api_token
  puts "Logged in as #{user.uid}"
rescue
end
