#!/bin/bash

set -eo pipefail

apk add --update build-base libxml2-dev libxslt-dev postgresql-dev git
rm -rf /var/cache/apk/*
apk add -U netcat-openbsd ruby-nokogiri
## Reason this hack is needed: https://gitlab.com/gitlab-org/gitlab-runner/issues/1042#note_61788095
## Long story short, this is the only way "sibling" docker services can communicate with one
## another in gitlab ci, for now.
cp /etc/hosts $CI_PROJECT_DIR
nc -z registry.gitlab.com-norad-ui-api 9248
bundle config build.nokogiri --use-system-libraries
gem install bundler --no-ri --no-rdoc
echo "gem 'tzinfo-data', '~> 1.2018.5'" >> Gemfile
bundle install --without production
yarn install
bundle exec rails db:setup
mkdir tmp
bin/rake js:routes['tmp/routes.js']
