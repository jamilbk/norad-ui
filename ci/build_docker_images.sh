#!/bin/bash

set -eo pipefail

apk update
apk add git
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
git clone https://gitlab.com/norad/api.git
cd api
if git rev-parse --verify origin/$CI_COMMIT_REF_NAME
then
  git checkout origin/$CI_COMMIT_REF_NAME
fi
docker build -t registry.gitlab.com/norad/ui/api:$CI_COMMIT_REF_NAME -f spec/support/Dockerfile .
docker push registry.gitlab.com/norad/ui/api:$CI_COMMIT_REF_NAME
