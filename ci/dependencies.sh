#!/bin/bash

apt-get update
apt-get install -y apt-transport-https

echo "Adding keys for necessary debian repos..."
curl -sS https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
curl -sS https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

echo "Adding repo sources list"
echo 'deb https://deb.nodesource.com/node_6.x jessie main' > /etc/apt/sources.list.d/nodesource.list
echo 'deb-src https://deb.nodesource.com/node_6.x jessie main' >> /etc/apt/sources.list.d/nodesource.list
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | tee /etc/apt/sources.list.d/google.list

echo "Installing Node, Yarn, and Google Chrome"
apt-get update
apt-get install -y nodejs yarn google-chrome-stable --no-install-recommends

echo "Installing bundler..."
gem install bundler

echo "Installing Gems from Gemfile..."
bundle install --jobs=4 --without production:development
