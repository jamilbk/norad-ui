const environment = require('./environment')
const merge = require('webpack-merge')
const path = require('path')
const webpack = require('webpack')

// Prevent karma from writing to filesystem during testing
environment.plugins.get('Manifest').opts.writeToFileEmit = process.env.NODE_ENV !== 'test'

// Configure istanbul coverage reporter
environment.loaders.set('istanbul-instrumenter', {
  test: /app\/javascript/,
  enforce: "post",
  loader: "istanbul-instrumenter-loader",
  query: {
    esModules: true
  },
  exclude: [
    'node_modules',
    'tmp',
    'spec'
  ]
})

const config = {
  resolve: {
    alias: {
      axios: path.resolve(__dirname, '../../app/javascript/lib/axios_interceptor.js'),
      NodeModules: path.resolve(__dirname, '../../node_modules'),
      helpers: path.resolve(__dirname, '../../spec/javascript/helpers'),
      Routes: path.resolve(__dirname, '../../tmp/routes.js')
    }
  },
  plugins: [
    new webpack.ProvidePlugin({ 'Routes': 'Routes' })
  ]
}

module.exports = merge(environment.toWebpackConfig(), config)
