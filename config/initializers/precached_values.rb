# frozen_string_literal: true

module PrecachedValues
  def self.translation_hash
    return_hash = YAML.load_file(Rails.root.join('config', 'application_type_display_names.yml'))
    return_hash
  end

  def self.application_type_options
    translation_hash.map do |id, name_hash|
      name_hash.map do |port, port_hash|
        port_hash.map do |protocol, name|
          display_port = name_hash.length > 1 ? "(#{port})" : ''
          display_name = name.present? ? "#{id}#{display_port}: #{name}" : id.to_s
          { id: id, display_name: display_name, port: port, protocol: protocol }
        end
      end
    end.flatten
  end

  def self.retrieve_translation_hash
    Rails.cache.fetch 'translation_hash' do
      translation_hash
    end
  end

  def self.retrieve_application_type_options
    Rails.cache.fetch 'application_type_options' do
      application_type_options
    end
  end
end
