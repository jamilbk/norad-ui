# frozen_string_literal: true

# Be sure to restart your server when you modify this file.
SESSION_TIMEOUT = 60.minutes
Rails.application.config.session_store :cookie_store,
                                       key: '_norad-ui_session',
                                       secure: Rails.env.production?,
                                       expire_after: SESSION_TIMEOUT

Rails.application.config.x.session_timeout = SESSION_TIMEOUT
