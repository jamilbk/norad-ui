# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180607192940) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "broadcast_messages", force: :cascade do |t|
    t.string "text"
    t.string "severity", default: "warning"
    t.boolean "live"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["live"], name: "index_broadcast_messages_on_live", unique: true, where: "(live = true)"
  end

  create_table "user_profiles", id: false, force: :cascade do |t|
    t.string "user_uid"
    t.string "default_org"
    t.index ["user_uid"], name: "index_user_profiles_on_user_uid", unique: true
  end

end
