class CreateBroadcastMessage < ActiveRecord::Migration[5.1]
  def change
    create_table :broadcast_messages do |t|
      t.string :text
      t.string :severity, default: 'warning'
      t.boolean :live
      t.timestamps(null: false)
      t.index :live, unique: true, where: "live = 't'"
    end
  end
end
