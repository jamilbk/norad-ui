class CreateUserProfile < ActiveRecord::Migration[4.2]
  def change
    create_table :user_profiles, id: false do |t|
      t.string :user_uid
      t.string :default_org
    end
    add_index(:user_profiles, :user_uid, unique: true)
  end
end
